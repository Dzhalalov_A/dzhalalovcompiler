package parser;

import org.junit.jupiter.api.Test;
import syntaxtree.DeclarationsNode;

import static org.junit.Assert.assertEquals;

public class ParserTreeJTest {

    /**
     * ProgramNode test on bitcoin.pas program
     */
    @Test
    public void ProgramNodeReturnFullTest(){
        //SyntaxTree for bitcoin example
        String expected =   "Program: foo\n" +
                            "|-- Declarations\n" +
                            "|-- --- Name: fee\n" +
                            "|-- --- Type: Integer\n" +
                            "|-- --- Name: fi\n" +
                            "|-- --- Type: Integer\n" +
                            "|-- --- Name: fo\n" +
                            "|-- --- Type: Integer\n" +
                            "|-- --- Name: fum\n" +
                            "|-- --- Type: Integer\n" +
                            "|-- SubProgram Declarations\n" +
                            "|-- --- Sub Program: function\n" +
                            "|-- --- --- Parameters\n" +
                            "|-- --- --- --- Name: x\n" +
                            "|-- --- --- --- Type: Integer\n" +
                            "|-- --- --- Declarations\n" +
                            "|-- --- --- --- Name: boo\n" +
                            "|-- --- --- --- Type: Real\n" +
                            "|-- --- --- --- Name: foo\n" +
                            "|-- --- --- --- Type: Real\n" +
                            "|-- --- --- SubProgram Declarations\n" +
                            "|-- --- --- Compound Statement\n" +
                            "|-- --- --- --- Assignment\n" +
                            "|-- --- --- --- --- Name: x\n" +
                            "|-- --- --- --- --- Type: null\n" +
                            "|-- --- --- --- --- Value: 74\n" +
                            "|-- --- --- --- --- Type: Integer\n" +
                            "|-- Compound Statement\n" +
                            "|-- --- Assignment\n" +
                            "|-- --- --- Name: fee\n" +
                            "|-- --- --- Type: null\n" +
                            "|-- --- --- Value: 4.0\n" +
                            "|-- --- --- Type: Real\n" +
                            "|-- --- Assignment\n" +
                            "|-- --- --- Name: fi\n" +
                            "|-- --- --- Type: null\n" +
                            "|-- --- --- Value: 5.2E12\n" +
                            "|-- --- --- Type: Real\n" +
                            "|-- --- Assignment\n" +
                            "|-- --- --- Name: fo\n" +
                            "|-- --- --- Type: null\n" +
                            "|-- --- --- Operation: PLUS\n" +
                            "|-- --- --- --- Operation: MULT\n" +
                            "|-- --- --- --- --- Value: 3\n" +
                            "|-- --- --- --- --- Type: Integer\n" +
                            "|-- --- --- --- --- Name: fee\n" +
                            "|-- --- --- --- --- Type: null\n" +
                            "|-- --- --- --- Name: fi\n" +
                            "|-- --- --- --- Type: null\n" +
                            "|-- --- If\n" +
                            "|-- --- --- Operation: LT\n" +
                            "|-- --- --- --- Name: fo\n" +
                            "|-- --- --- --- Type: null\n" +
                            "|-- --- --- --- Value: 13\n" +
                            "|-- --- --- --- Type: Integer\n" +
                            "|-- --- --- Assignment\n" +
                            "|-- --- --- --- Name: fo\n" +
                            "|-- --- --- --- Type: null\n" +
                            "|-- --- --- --- Value: 13\n" +
                            "|-- --- --- --- Type: Integer\n" +
                            "|-- --- --- Assignment\n" +
                            "|-- --- --- --- Name: fo\n" +
                            "|-- --- --- --- Type: null\n" +
                            "|-- --- --- --- Value: 26\n" +
                            "|-- --- --- --- Type: Integer\n" +
                            "|-- --- I/O write\n" +
                            "|-- --- --- Name: fo\n" +
                            "|-- --- --- Type: null\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "program foo;\n" +
                "var fee, fi, fo, fum: integer;\n" +
                "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                "begin\n" +
                "  fee := 4.0;\n" +
                "  fi := 5.2E12;\n" +
                "  fo := 3 * fee + fi;\n" +
                "  if fo < 13\n" +
                "    then\n" +
                "      fo := 13\n" +
                "    else\n" +
                "      fo := 26\n" +
                "  ;\n" +
                "  write( fo)\n" +
                "end\n" +
                ".\n", false);
        //Converting ProgramNode received from parser
        String actual = pr.programRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * Factor Value node return test
     */
    @Test
    public void FactorNodeValueTest(){
        //SyntaxTree for simplest.pas
        String expected = "Value: 123\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "123", false);
        //Converting ProgramNode received from parser
        String actual = pr.factorRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }
    /**
     * Factor Variable node return test
     */
    @Test
    public void FactorNodeVariableTest(){
        //SyntaxTree for simplest.pas
        String expected = "Name: foo\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "foo", false);
        //Converting ProgramNode received from parser
        String actual = pr.factorRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * SimpleExpression node return test
     */
    @Test
    public void SimpleExpressionNodeTest(){
        //SyntaxTree for simplest.pas
        String expected =   "Operation: PLUS\n" +
                "|-- Name: foo\n" +
                "|-- Name: bar\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "foo + bar", false);
        //Converting ProgramNode received from parser
        String actual = pr.simpleExpressionRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * Statement node return test
     */
    @Test
    public void StatementNodeTest(){
        //SyntaxTree for simplest.pas
        String expected =   "If\n" +
                "|-- Operation: GT\n" +
                "|-- --- Name: foo\n" +
                "|-- --- Name: bar\n" +
                "|-- I\\O read\n" +
                "|-- --- Name: input\n" +
                "|-- I\\O write\n" +
                "|-- --- Name: foo\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "if foo > bar then read(input) else write(foo);", false);
        //Converting ProgramNode received from parser
        String actual = pr.statementRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * Statement node return test
     */
    @Test
    public void SubProgramDeclarationNodeTest(){
        //SyntaxTree for simplest.pas
        String expected =   "If\n" +
                "|-- Operation: GT\n" +
                "|-- --- Name: foo\n" +
                "|-- --- Name: bar\n" +
                "|-- I\\O read\n" +
                "|-- --- Name: input\n" +
                "|-- I\\O write\n" +
                "|-- --- Name: foo\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser( "if foo > bar then read(input) else write(foo);", false);
        //Converting ProgramNode received from parser
        String actual = pr.statementRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * Declarations node return test
     */
    @Test
    public void DeclarationNodeTest(){
        String expected =   "Declarations\n" +
                "|-- Name: dollars\n" +
                "|-- Name: yen\n" +
                "|-- Name: bitcoins\n" +
                "|-- Name: fee\n" +
                "|-- Name: fi\n";
        Parser pr = new Parser("var dollars, yen, bitcoins: integer; var fee, fi: real;", false);
        DeclarationsNode declarations = new DeclarationsNode();     //Creates Declaration Node
        pr.declarationsRecognizer(declarations);
        String actual = declarations.indentedToString(0);
        assertEquals(expected, actual);
    }
}
