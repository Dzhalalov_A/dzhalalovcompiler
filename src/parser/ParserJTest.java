package parser;

import org.junit.jupiter.api.Test;
import static org.junit.Assert.*;
import syntaxtree.DeclarationsNode;
import syntaxtree.SubProgramDeclarationsNode;

/**
	Unit tests of the Parser class, to be run with JUnit.
 */
public class ParserJTest {

    /**
     * Program recognizer test
     */
    @Test
    public void testProgramRecognizer() {
        System.out.println("Testing programRecognizer");
        //Simplest Pascal program
        String input =  "program foo;\n" +
                        "var fee, fi: integer;\n" +
                        "begin \n" +
                        "fee := 3;\n" +
                        "write(fee + fi);\n" +
                        "fi := 5\n" +
                        "end\n" +
                        ".";;
        Parser p = new Parser(input, false);
        try {
            p.programRecognizer();
        } catch (Exception e) { /* End of String or File handler */ }
        System.out.println("programRecognizer test passed!");
    }

    /**
     * Program recognizer syntax tree test
     */
    @Test
    public void testProgramRecognizerSyntaxTree() {
        System.out.println("Testing programRecognizer syntax tree");
        //SyntaxTree for simplest.pas
        String expected =   "Program: foo" + "\n" +
                            "|-- Declarations" + "\n" +
                            "|-- SubProgram Declarations" + "\n" +
                            "|-- Compound Statement" + "\n";

        //Simplest Pascal program
        String input = "program foo; begin end .";
        Parser p = new Parser(input, false);
        String actual = p.programRecognizer().indentedToString(0);
        assertEquals(expected, actual);
        System.out.println("programRecognizer syntax tree test passed!");
    }

    /**
     * Declarations recognizer test
     */
    @Test
    public void testDeclarationsRecognizer() {

        Parser p = new Parser("var fee, fi, fo, fum: integer;   ", false);
        try {
            DeclarationsNode dn = new DeclarationsNode();
            p.declarationsRecognizer(dn);
        } catch (NullPointerException e) {

        }
        System.out.println("declarationsRecognizer test passed!");
    }

    /**
     * subprogramDeclarationsTest
     */
    @Test
    public void testSubprogramDeclarations() {
        try{
            Parser p = new Parser("procedure id ; begin end ;", false);
            SubProgramDeclarationsNode subPD = new SubProgramDeclarationsNode();
            p.subprogramDeclarationRecognizer(subPD);
        }
        catch (NullPointerException e){}

        System.out.println("subprogramDeclarationsRecognizer test passed!");
    }

    /**
     * Statement recognizer test
     */

    @Test
    public void testStatementRecognizer() {
        /*Things to test
            1. variable assignop expression                 TESTED!
            2. procedure_statement                          TESTED!
            3. compound_statement                           TESTED!
            4. if expression then statement else statement  TESTED!
            5. while expression do statement                TESTED!
            6. read ( id )
            7. write ( expression )
        */
        Parser p = new Parser("begin end", false);

        //1. Variable
        try {
            p = new Parser("foo := 5 + foo", false);
            p.st.addVariableName("foo");
            p.statementRecognizer();
        } catch (Exception e) { /* End of String or File handler */ }

        //2. Procedure
        try {
            p = new Parser("LessThan(foo < bar)", false);
            p.st.addProcedureName("LessThan");
            p.statementRecognizer();
        } catch (Exception e) { /* End of String or File handler */ }

        //3. Compound statement without optional_statements
        try {
            p = new Parser("begin end", false);
            p.statementRecognizer();
        } catch (Exception e) { /* End of String or File handler */ }

        //4. If then else
        try {
            p = new Parser("if foo > bar then begin fee end else free", false);
            p.st.addVariableName("foo");
            p.st.addVariableName("bar");
            p.st.addProcedureName("fee");
            p.st.addProcedureName("free");
            p.statementRecognizer();
        } catch (Exception e) { /* End of String or File handler */ }

        System.out.println("statementRecognizer test passed!");
    }

    /**
     * simpleExpressionRecognizer test
     */
    @Test
    public void testSimpleExpressionRecognizer() {

        Parser p = new Parser("", false);
        try {
            p = new Parser("foo * bar + foobar", false);
            p.simpleExpressionRecognizer();
        } catch (NullPointerException e) { /* End of String or File handler */ }

        try {
            p = new Parser("+foo * bar - foobar", false);
            p.simpleExpressionRecognizer();
        } catch (NullPointerException e) { /* End of String or File handler */ }

        System.out.println("simpleExpressionRecognizer test passed!");
    }

    /**
     * factorRecognizer test
     */
    @Test
    public void testFactorRecognizer() {
        /*
            Things to test
            1. ID                   TESTED!
            2. ID [ expression ]    TESTED!
            3. ID ( expression )    TESTED!
            4. NUM                  TESTED!
            5. ( expression )       TESTED!
            6. NOT factor           TESTED!
        */

        Parser p = new Parser("", false);
        //"ID" production rule
        try {
            p = new Parser("Marvel", false);
            p.factorRecognizer();

        } catch (NullPointerException e) { /* Do Nothing :D */ }

        //"id[expression]" production rule
        p = new Parser("Bat [ Man ]", false);

        //id(expression) production rule
        p = new Parser("Spider ( Man )", false);

        //"NUM" production rule
        try {
            p = new Parser("21", false);
            p.factorRecognizer();

        } catch (NullPointerException e) { /* Do Nothing :D */ }

        //"(expression)" production rule
        p = new Parser("( DeadPool )", false);
        p.factorRecognizer();

        //"NOT" production rule
        p = new Parser("not 41", false);
        p.factorRecognizer();

        System.out.println("factorRecognizer test passed!");
    }

    /*
     * Failure methods will exit with code 999, because their input strings will not trigger any If statement.
     * Invalid input will result in error in subsequent call of the match() method
     */
    //@Rule
    //public final ExpectedSystemExit exit = ExpectedSystemExit.none(); //Attempt to handle System.Exit and prevent JUnit from stopping.
    /*
     * Failing procedureRecognizer
     */
    /*REMOVE ME FOR FAILURE TEST
    @Test
    public void procedureFailureTest(){
        Parser p = new Parser("LessThan(foo < bar)", false);
        p.st.addProcedureName("LessThan");
        p.statementRecognizer();
    }
    ME TOO!*/

    /*
     * Failing procedureRecognizer
     */

    /*REMOVE ME FOR FAILURE TEST
    @Test
    public void variableFailureTest(){
        Parser p = new Parser("foo := 5 + foo", false);
        p.st.addProgramName("foo");
        p.statementRecognizer();
    }
    ME TOO!*/
}