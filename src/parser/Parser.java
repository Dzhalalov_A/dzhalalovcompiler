package parser;

import java.io.*;
import java.util.ArrayList;
import scanner.Scanner;
import scanner.Token;
import scanner.TokenType;
import symboltable.SymbolTable;
import syntaxtree.*;


/**
 * The parser recognizes whether an input string of tokens is a valid Pascal program.
 * @author Artur Dzhalalov
 */
public class Parser {
    /*
     * Variables made public to make testing easy
     */
    public Token lookAhead;
    public Scanner scanner;
    public SymbolTable st;
    private DeclarationsNode declaredVars;  //used to give methods access to the variable type
    /**
     * Parser constructor.
     * Creates parser object. When calling parser constructor set isFileName to true
     * if text contains path to a file otherwise text will be used as an input string
     * @param text       string containing program snippet or file name
     * @param isFileName boolean flag defines if "text" is a file name
     */
    public Parser (String text, boolean isFileName){

        if(isFileName){
            FileInputStream fis = null;
            try{
                fis = new FileInputStream(text);
            }
            catch (FileNotFoundException e){
                System.out.println("File not found");
            }

            InputStreamReader isr = new InputStreamReader(fis);
            scanner = new Scanner(isr);
            try{
                lookAhead = scanner.nextToken();
            }
            catch (IOException e){
                System.out.println("Scanner error in file mode");
            }
            st = new SymbolTable();
        }
        else{
            st = new SymbolTable();
            scanner = new Scanner(new StringReader(text));
            try{
                lookAhead = scanner.nextToken();
            } catch (IOException e){
                System.out.println("Scanner error in text mode");
            }
        }
    }

     ///////////////
    // RECOGNIZERS //
     ///////////////

    /**
     * Recognizes program expression and returns Program Node
     * @return pn Returns a root of the syntax tree
     */
    public ProgramNode programRecognizer(){
        match(TokenType.PROGRAM);
        this.st.addProgramName(lookAhead.getLexeme()); //adding program id to a SymbolTable
        ProgramNode pn = new ProgramNode(lookAhead.getLexeme());   //creating a Program node
        match(TokenType.ID);
        match(TokenType.SEMI);
        DeclarationsNode variables = new DeclarationsNode();
        declarationsRecognizer(variables);
        this.declaredVars = variables;
        SubProgramDeclarationsNode functions  = new SubProgramDeclarationsNode();
        subprogramDeclarationsRecognizer(functions);
        CompoundStatementNode main = new CompoundStatementNode();
        compoundStatementRecognizer(main);
        pn.setDeclarations(variables);
        pn.setFunctions(functions);
        pn.setMain(main);
        match(TokenType.PERIOD);
        return pn;
    }

    /**
     * Recognizes identifier list expression
     * @param variables A node that holds a collection of variables
     */
    public void identifierListRecognizer(DeclarationsNode variables){
        this.st.addVariableName(lookAhead.getLexeme()); //adding variable id to a SymbolTable
        VariableNode temp = new VariableNode(this.lookAhead.getLexeme());
        variables.addVariable(temp);
        match(TokenType.ID);
        if(this.lookAhead.getType() == TokenType.COMMA) {
            match(TokenType.COMMA);
            identifierListRecognizer(variables);
        }
    }

    /**
     * Recognizes declarations expression
     * @param variables A node that holds a collection of variables
     */
    public void declarationsRecognizer(DeclarationsNode variables){
        if(this.lookAhead.getType() == TokenType.VAR) {
            match(TokenType.VAR);
            DeclarationsNode temp = new DeclarationsNode(); //temp node to hold variables of one type
            identifierListRecognizer(temp);
            match(TokenType.COLUMN);
            ExpressionType tempType = typeRecognizer();
            ArrayList<VariableNode> tempList = temp.getVariables();
            for(VariableNode var : tempList){
                var.setType(tempType);      //setting up the type of variable node
                variables.addVariable(var); //adding variable to declarations node
            }
            match(TokenType.SEMI);
            declarationsRecognizer(variables);
        }
        else{
            //Lambda
        }
    }

    /**
     * Recognizes type expression
     */
    public ExpressionType typeRecognizer(){
        ExpressionType answer = null;
        if(this.lookAhead.getType() == TokenType.ARRAY){
            match(TokenType.ARRAY);
            match(TokenType.LBRACK);
            match(TokenType.INT);
            match(TokenType.COLUMN);
            match(TokenType.INT);
            match(TokenType.RBRACK);
            match(TokenType.OF);
            answer = standardTypeRecognizer();
        }
        else{
            answer = standardTypeRecognizer();
        }
        return answer;
    }

    /**
     * Recognizes standard_type expression
     */
    public ExpressionType standardTypeRecognizer(){
        ExpressionType answer = null;
        if(this.lookAhead.getType() == TokenType.REAL){
            match(TokenType.REAL);
            answer = ExpressionType.Real;
        }
        else if(this.lookAhead.getType() == TokenType.INTEGER){
            match(TokenType.INTEGER);
            answer = ExpressionType.Integer;
        }
        else{
            error("invalid standard type");
        }
        return answer;
    }

    /**
     * Recognizes subprogram_declarations expression
     * @param functions A node that holds a collection of sub program declarations
     */
    public void subprogramDeclarationsRecognizer(SubProgramDeclarationsNode functions){
        //check if next exp is sub_prog followed by sub_prog head
        if(this.lookAhead.getType() == TokenType.FUNCTION || this.lookAhead.getType() == TokenType.PROCEDURE){
            subprogramDeclarationRecognizer(functions);
            match(TokenType.SEMI);
            subprogramDeclarationsRecognizer(functions);
        }
        else{
            //Lambda
        }
    }

    /**
     * Recognizes subprogram_declaration expression
     * @param subs collection of sub program declarations
     */
    public void subprogramDeclarationRecognizer(SubProgramDeclarationsNode subs){
        //Initializing subprogram
        SubProgramNode subP = new SubProgramNode();
        subprogramHeadRecognizer(subP);

        DeclarationsNode subVariables = new DeclarationsNode();
        SubProgramDeclarationsNode subFunctions = new SubProgramDeclarationsNode();
        CompoundStatementNode subMain = new CompoundStatementNode();

        //Initializing subprogram's components
        subprogramHeadRecognizer(subP);
        declarationsRecognizer(subVariables);
        subprogramDeclarationsRecognizer(subFunctions);
        compoundStatementRecognizer(subMain);

        //Setting up subporgram's declarations, subprogram_declarations, and compound_statements
        subP.setVariables(subVariables);
        subP.setFunctions(subFunctions);
        subP.setMain(subMain);

        //Adding current subProgram to SubProgram Declarations
        subs.addSubProgramDeclaration(subP);
    }

    /**
     * Recognizes subprogram_head expression
     * @param subP A SubProgramNode holding a reference to a sub program
     */
    public void subprogramHeadRecognizer(SubProgramNode subP){
        if(this.lookAhead.getType() == TokenType.FUNCTION){
            this.st.addFunctionName(lookAhead.getLexeme()); //adding function id to a SymbolTable
            subP.setName(this.lookAhead.getLexeme()); //setting up the name of the function
            match(TokenType.FUNCTION);
            match(TokenType.ID);
            argumentsRecognizer(subP);
            match(TokenType.COLUMN);
            standardTypeRecognizer();
            match(TokenType.SEMI);
        }
        else if(this.lookAhead.getType() == TokenType.PROCEDURE){
            this.st.addProcedureName(this.lookAhead.getLexeme()); //adding procedure id to a SymbolTable
            subP.setName(this.lookAhead.getLexeme());
            match(TokenType.PROCEDURE);
            match(TokenType.ID);
            argumentsRecognizer(subP);
            match(TokenType.SEMI);
        }
    }

    /**
     * Recognizes arguments expression
     * @param subP A SubProgramNode holding a reference to a sub program
     */
    public void argumentsRecognizer(SubProgramNode subP){
        if(this.lookAhead.getType() == TokenType.LPAREN){
            match(TokenType.LPAREN);
            parameterListRecognizer(subP);
            match(TokenType.RPAREN);
        }
        else
        {
            //Lambda
        }

    }

    /**
     * Recognizes parameter_list expression
     * @param subP A SubProgramNode holding a reference to a sub program
     */
    public void parameterListRecognizer(SubProgramNode subP){
        DeclarationsNode temp = new DeclarationsNode(); //temp node to hold variables of one type
        identifierListRecognizer(temp);
        match(TokenType.COLUMN);
        ExpressionType tempType = typeRecognizer();
        ArrayList<VariableNode> tempList = temp.getVariables();
        ParametersNode tempParameters = subP.getParameters(); //extracting parametes for modification
        for(VariableNode var : tempList){
            var.setType(tempType);      //setting up the type of variable node
            tempParameters.addVariable(var); //adding variable to declarations node
        }
        subP.setParameters(tempParameters); //assigning new parameters node to SubProgram
        if(this.lookAhead.getType() == TokenType.SEMI){
            match(TokenType.SEMI);
            parameterListRecognizer(subP);
        }
    }

    /**
     * Recognizes compound_statement expression
     * @param statements A CompoundStatementNode holding a reference to a statements collection of program or sub program
     */
    public void compoundStatementRecognizer(CompoundStatementNode statements){
        match(TokenType.BEGIN);
        optionalStatementsRecognizer(statements);
        match(TokenType.END);
        return;
    }

    /**
     * Recognizes optional_statements expression
     * @param statements A CompoundStatementNode holding a reference to a statements collection of program or sub program
     */
    public void optionalStatementsRecognizer(CompoundStatementNode statements){
        if(this.lookAhead.getType() == TokenType.ID ||
                this.lookAhead.getType() == TokenType.BEGIN ||
                this.lookAhead.getType() == TokenType.IF ||
                this.lookAhead.getType() == TokenType.WHILE ||
                this.lookAhead.getType() == TokenType.WRITE ||
                this.lookAhead.getType() == TokenType.READ){
            statementListRecognizer(statements);
        }
        else{
            //Lambda
        }
    }

    /**
     * Recognizes statement_list expression
     * @param statements A CompoundStatementNode holding a reference to a statements collection of program or sub program
     */
    public void statementListRecognizer(CompoundStatementNode statements){
        statements.addStatement(statementRecognizer());
        if(this.lookAhead.getType() == TokenType.SEMI){
            match(TokenType.SEMI);
            statementListRecognizer(statements);
        }
    }

    /**
     * Recognizes statement expression
     * @return answer Returns a statement node
     */
    public StatementNode statementRecognizer(){
        StatementNode answer = null;
        if(this.lookAhead.getType() == TokenType.ID){
            if(st.isVariableName(this.lookAhead.getLexeme())){ //testing if next id is a variable name
                //Setting up assignment node
                AssignmentStatementNode temp = new AssignmentStatementNode();
                temp.setLvalue(variableRecognizer());
                //Assign expression type
                match(TokenType.ASSIGN);
                temp.setExpression(expressionRecognizer());
                answer = temp;
            }
            else if(st.isArrayName(this.lookAhead.getLexeme())){ //testing if next id is a array name
                variableRecognizer();
                match(TokenType.ASSIGN);
                expressionRecognizer();
            }
            else if(st.isProcedureName(this.lookAhead.getLexeme())){ //testing if next id is a procedure name
                answer = procedureStatementRecognizer();
            }
            else {
                error("Invalid ID type");
            }
        }
        else if(this.lookAhead.getType() == TokenType.BEGIN){
            CompoundStatementNode temp = new CompoundStatementNode();
            compoundStatementRecognizer(temp);
            answer = temp;
        }
        else if(this.lookAhead.getType() == TokenType.IF){
            IfStatementNode temp = new IfStatementNode();
            match(TokenType.IF);
            temp.setTest(expressionRecognizer());
            match(TokenType.THEN);
            temp.setThenStatement(statementRecognizer());
            match(TokenType.ELSE);
            temp.setElseStatement(statementRecognizer());
            answer = temp;
        }
        else if(this.lookAhead.getType() == TokenType.WHILE){
            WhileStatementNode temp = new WhileStatementNode();
            match(TokenType.WHILE);
            temp.setTest(expressionRecognizer());
            match(TokenType.DO);
            temp.setLoop(statementRecognizer());
            answer = temp;
        }
        else if(this.lookAhead.getType() == TokenType.READ){
            IOStatementNode temp = new IOStatementNode();
            temp.setOperation("read");
            match(TokenType.READ);
            match(TokenType.LPAREN);
            temp.setVarName(this.lookAhead.getLexeme());
            VariableNode var = new VariableNode(this.lookAhead.getLexeme());
            temp.setAttribute(var);
            match(TokenType.ID);
            match(TokenType.RPAREN);
            answer = temp;
        }
        else if(this.lookAhead.getType() == TokenType.WRITE){
            IOStatementNode temp = new IOStatementNode();
            temp.setOperation("write");
            match(TokenType.WRITE);
            match(TokenType.LPAREN);
            temp.setAttribute(expressionRecognizer());
            match(TokenType.RPAREN);
            answer = temp;
        }
        else{
            error("Invalid statement");
        }

        return answer;
    }

    /**
     * Recognizes variable expression
     * @return answer returns a variable node
     */
    public VariableNode variableRecognizer(){
        VariableNode answer = new VariableNode(this.lookAhead.getLexeme());
        if(declaredVars != null){
            for(VariableNode var : declaredVars.getVariables()){
                if( var.getName().equals(answer.getName())){
                    answer.setType(var.getType());  //setting type of answer according to the type of the corresponding node in declarations
                }
            }
        }
        match(TokenType.ID);
        return answer;
    }

    /**
     * Recognizes procedure_statement expression
     */
    public ProcedureNode procedureStatementRecognizer(){
        ProcedureNode answer = null;
        answer.setName(this.lookAhead.getLexeme());
        match(TokenType.ID);
        if(this.lookAhead.getType() == TokenType.LPAREN){
            match(TokenType.LPAREN);
            ExpressionListNode expList = new ExpressionListNode();
            answer.setExpressions(expressionListRecognizer(expList));
            match(TokenType.RPAREN);
        }
        return answer;
    }

    /**
     * Recognizes expression_list expression
     * @param expList A ExpressionListNode holds a references to a expressions collection
     * @return expList A ExpressionListNode holds a references to a expressions collection
     */
    public ExpressionListNode expressionListRecognizer(ExpressionListNode expList){
        expList.addExpressionNode(expressionRecognizer());
        if(this.lookAhead.getType() == TokenType.COMMA){
            match(TokenType.COMMA);
            expressionListRecognizer(expList);
        }
        return expList;
    }

    /**
     * Recognizes "expression" expression
     * @return answer Expression node that represents a simple expression
     */
    public ExpressionNode expressionRecognizer(){
        ExpressionNode answer = null;
        answer = simpleExpressionRecognizer();
        TokenType temp = this.lookAhead.getType(); //temp stores TokenType of current token
        if(isRelOp(this.lookAhead)){
            OperationNode simpleExpression = new OperationNode(temp);
            simpleExpression.setLeft(answer);
            simpleExpression.setRight(simpleExpressionRecognizer());
            answer = simpleExpression;
        }
        return answer;
    }

    /**
     * Recognizes simple_expression expression
     * @return answer returns Expression node that represents a simple expression
     */
    public ExpressionNode simpleExpressionRecognizer(){
        ExpressionNode answer = null;
        OperationNode simplePart = null;
        UnaryOperationNode temp = null;
        switch (this.lookAhead.getType()){
            case ID:
            case INT:
            case FLOAT:
            case EXP:
            case LPAREN:
                answer = termRecognizer();
                simplePart = simplePartRecognizer();
                if(simplePart != null){
                    simplePart.setLeft(answer);
                    answer = simplePart;
                }
                break;
            case NOT:
                temp = new UnaryOperationNode(TokenType.NOT);
                answer = termRecognizer();  //answer holds return value of term
                temp.setOperand(answer);    //answer is set as temp's child node
                simplePart = simplePartRecognizer();
                if(simplePart != null){             //simplePart is not empty
                    simplePart.setLeft(answer);     // therefore temp's child node is an expression combined from term and simple part
                    temp.setOperand(simplePart);
                }
                answer = temp;
                break;
            case PLUS:
            case MINUS:
                temp = signRecognizer();
                answer = termRecognizer();  //answer holds return value of term
                temp.setOperand(answer);    //answer is set as temp's child node
                simplePart = simplePartRecognizer();
                if(simplePart != null){             //simplePart is not empty
                    simplePart.setLeft(answer);     // therefore temp's child node is an expression combined from term and simple part
                    temp.setOperand(simplePart);
                }
                answer = temp;
                break;
            default:
                error("Invalid simple_expression");
        }
        return answer;
    }

    /**
     * Recognizes simple_part expression
     * @return answer returns Operation node that represents simple part expression
     */
    public OperationNode simplePartRecognizer(){
        OperationNode answer = null;
        OperationNode simplePart = null;
        TokenType temp = this.lookAhead.getType(); //temp stores TokenType of current token
        if(isAddOp(this.lookAhead)){
            answer = new OperationNode(temp);
            answer.setRight(termRecognizer());
            simplePart = simplePartRecognizer();
            if(simplePart != null){
                simplePart.setLeft(answer);
                answer.setRight(simplePart);
            }

        }
        else{
            //Lambda
        }

        return answer;
    }

    /**
     * Recognizes term expression
     * @return answer Expression node that represents a simple expression
     */
    public ExpressionNode termRecognizer(){
        ExpressionNode answer = null;
        OperationNode termPart = null;
        answer = factorRecognizer();
        ExpressionNode exp = termPartRecognizer();
        if(exp instanceof OperationNode){
            termPart = (OperationNode) exp;
            termPart.setLeft(answer); //setting left part of operation node returned from termPart
            answer = termPart;
        }
        return answer;
    }

    /**
     * Recognizes term_part expression
     * @return answer An Expression node representing a single var, value or empty node OR a complex Operation Node
     */
    public OperationNode termPartRecognizer(){
        OperationNode answer = null;
        OperationNode termPart = null;
        TokenType temp = this.lookAhead.getType(); //temp stores TokenType of current token
        if(isMulOp(this.lookAhead)){
            answer = new OperationNode(temp);
            answer.setRight(factorRecognizer());
            termPart = termPartRecognizer();
            if(termPart != null){
                termPart.setLeft(answer.getRight());
                answer.setRight(termPart);
            }
        }
        else{
            //Lambda
        }
        return answer;
    }

    /**
     * Recognizes factor expression
     * @return answer Expression node that represents a simple expression
     */
    public ExpressionNode factorRecognizer(){
        ExpressionNode answer = null;
        if(this.lookAhead.getType() == TokenType.ID){
            answer = variableRecognizer();                                       //variable
            if(this.lookAhead.getType() == TokenType.LBRACK){           //Array
                match(TokenType.LBRACK);
                expressionRecognizer();
                match(TokenType.RBRACK);
            }else if(this.lookAhead.getType() == TokenType.LPAREN){     //Procedure
                match(TokenType.LPAREN);
                ExpressionListNode expList = new ExpressionListNode();
                expressionListRecognizer(expList);
                answer = expList;
                match(TokenType.RPAREN);
            }
        }else if(this.lookAhead.getType() == TokenType.INT){        //Integer value node
            ValueNode temp = new ValueNode(lookAhead.getLexeme());
            temp.setType(ExpressionType.Integer);
            answer = temp;
            match(TokenType.INT);
        }else if(this.lookAhead.getType() == TokenType.FLOAT){      //Float value node
            ValueNode temp = new ValueNode(lookAhead.getLexeme());
            temp.setType(ExpressionType.Real);
            answer = temp;
            match(TokenType.FLOAT);
        }else if(this.lookAhead.getType() == TokenType.EXP){        //Exponent value node
            ValueNode temp = new ValueNode(lookAhead.getLexeme());
            temp.setType(ExpressionType.Real);
            answer = temp;
            match(TokenType.EXP);
        }else if(this.lookAhead.getType() == TokenType.LPAREN) {
            match(TokenType.LPAREN);
            answer = expressionRecognizer();
            match(TokenType.RPAREN);

        }else if(this.lookAhead.getType() == TokenType.NOT){
            match(TokenType.NOT);
            factorRecognizer();
        }else{
            error("Invalid factor");
        }
        return answer;
    }

    /**
     * Recognizes sign expression
     * answer returns UnaryOperation node that represents a unary operation: plus/minus signs or negation
     */
    public UnaryOperationNode signRecognizer(){
        UnaryOperationNode answer = null;
        if(this.lookAhead.getType() == TokenType.PLUS){
            answer = new UnaryOperationNode(TokenType.PLUS);
            match(TokenType.PLUS);
        }else if (this.lookAhead.getType() == TokenType.MINUS){
            answer = new UnaryOperationNode(TokenType.MINUS);
            match(TokenType.MINUS);
        }else{
            error("Invalid sign");
        }

        return answer;
    }


     /////////////
    // UTILITIES //
     /////////////
    /**
     * Match method will compare current token with expected token
     * @param expected Expected TokenType
     */
    public void match(TokenType expected){
        if(this.lookAhead.getType() == expected){
            try{
                this.lookAhead = scanner.nextToken();
                //EOF token handler
                if(this.lookAhead == null){
                    this.lookAhead = new Token(TokenType.EOF, null);
                    //System.out.println("End of File reached");
                }
            }
            catch(IOException e){
                System.out.println("Scanner error: match");
            }
        }
        else{
            //error message tokens don't match
            error("Expected " + expected + " found " + this.lookAhead.getType() + " instead.");
        }
    }

    /**
     * Prints an error message specifying recognizer method and then exits the program.
     * @param errMsg The error message.
     */

    //add getLine and getColumn to scanner
    public void error( String errMsg){
        System.out.println( "Parser: error on line " + (scanner.yyline + 1) + "\n" + errMsg + "\n");
        System.out.println("Symbol Table");
        System.out.println(st.toString());
        System.exit(0);
    }

    /**
     * Determines if current token belongs to Relational Operators.
     * If it is then it will consume it.
     *
     * @param t current token
     * @return true if the token is a relop, false otherwise
     */
    private boolean isRelOp(Token t){
        boolean check = false;
        switch (this.lookAhead.getType()){
            case EQUAL:     // ==
                match(TokenType.EQUAL);
                check = true;
                break;
            case NEQUAL:    // !=
                match(TokenType.NEQUAL);
                check = true;
                break;
            case LT:        // <
                match(TokenType.LT);
                check = true;
                break;
            case LTE:       // <=
                match(TokenType.LTE);
                check = true;
                break;
            case GT:        // >
                match(TokenType.GT);
                check = true;
                break;
            case GTE:       // >=
                match(TokenType.GTE);
                check = true;
                break;
        }
        return check;
    }

    /**
     * Determines if current token belongs to Addition Operators.
     * If it is then it will consume it.
     *
     * @param t current token
     * @return true if the token is an addop, false otherwise
     */
    private boolean isAddOp(Token t){
        boolean check = false;
        switch (this.lookAhead.getType()){
            case PLUS:
                match(TokenType.PLUS);
                check = true;
                break;
            case MINUS:
                match(TokenType.MINUS);
                check = true;
                break;
            case OR:
                match(TokenType.OR);
                check = true;
                break;
        }
        return check;
    }

    /**
     * Determines if current token belongs to Multiplication Operators.
     * If it is then it will consume it.
     *
     * @param t current token
     * @return true if the token is a addop, false otherwise
     */
    private boolean isMulOp(Token t){
        boolean check = false;
        switch (this.lookAhead.getType()){
            case MULT:
                match(TokenType.MULT);
                check = true;
                break;
            case DIV:
                match(TokenType.DIV);
                check = true;
                break;
            case MOD:
                match(TokenType.MOD);
                check = true;
                break;
            case AND:
                match(TokenType.AND);
                check = true;
                break;
        }
        return check;
    }
}
