import parser.Parser;
import java.io.*;
/**
 * This class is a facilitator of compiling process.
 */
public class Compiler {
    /**
     * Main method calls parser on the input file, it will report if parsing was successful or not, and it will create an output file.
     * @param args Command line arguments args[0] holds a filename of an input file.
     */
    public static void main (String[] args){

        //Creating new parser object
        Parser pr = new Parser(args[0], true);
        System.out.println(args[0]);

        //Parsing input file
        try{
            pr.programRecognizer();
            System.out.println("Parsed! :)");
        }
        catch (Exception e){
            System.out.println("Error! " + e);
            System.out.println("Didn't parse :(");
        }

        /* Printing out symbol table */

        //Formatting output file name
        String outputFileName = new String(args[0]);
        outputFileName = outputFileName.replace(".pas", "_SymbolTable.txt");
        System.out.print(outputFileName);
        //Writing Symbol table to a file
        try{
            PrintWriter pw = new PrintWriter(outputFileName);
            pw.print(pr.st.toString());
            pw.close();
        }
        catch (Exception e){
            System.out.print("Error! " + e);
        }
    }
}
