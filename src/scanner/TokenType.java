package scanner;

/** Associates TokenType with an integer value
 *	My_Scanner uses integer values to assign TokenTypes
 *	@author Artur Dzhalalov
 */

	public enum TokenType 
	{
		//Special Symbols
		SEMI, COMMA, PERIOD, COLUMN, LBRACK, RBRACK, LPAREN, RPAREN, PLUS, MINUS,
		EQUAL, NEQUAL, LT, LTE, GT, GTE, MULT, DIV, ASSIGN,
		
		//KeyWords
		AND, ARRAY, BEGIN, INTDIV, DO, ELSE, END, FUNCTION, IF, INTEGER,
		MOD, NOT, OF, OR, PROCEDURE, PROGRAM, REAL, THEN, VAR, WHILE,

        // Common types
		ID, INT, FLOAT, EXP, INVALID, EOF,

        // IO types
        READ, WRITE
	}
