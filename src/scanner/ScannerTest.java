package scanner;

import java.io.FileInputStream;
import java.io.InputStreamReader;

public class ScannerTest
{

    public static void main( String[] args) 
    {
        String filename = "./scanner/test.txt";
        FileInputStream fis = null;
        try 
		{
            fis = new FileInputStream( filename);
        } 
		catch (Exception e ) { e.printStackTrace();}
		
        InputStreamReader isr = new InputStreamReader( fis );
        Scanner scanner = new Scanner( isr );
        Token aToken = null;
        do
        {
			//trying to get a new token
            try 
			{
                aToken = scanner.nextToken();
            }
            catch( Exception e) { e.printStackTrace();}
            
			//printing current token, uses toString method from Token class
			System.out.println(aToken);
			
        } while( aToken != null);
    }
}

