package scanner;

/**
 * Lookup table used to assigned correct types to the input tokens
 * @author Artur Dzhalalov
 */

import java.util.HashMap;

public class LookUpTable extends HashMap<String, TokenType>{
	
	public LookUpTable(){
		//Special symbols
		this.put(";",  TokenType.SEMI);
		this.put(",",  TokenType.COMMA);
		this.put(".",  TokenType.PERIOD);
		this.put(":",  TokenType.COLUMN);
		this.put("[",  TokenType.LBRACK);
		this.put("]",  TokenType.RBRACK);
		this.put("(",  TokenType.LPAREN);
		this.put(")",  TokenType.RPAREN);
		this.put("+",  TokenType.PLUS);
		this.put("-",  TokenType.MINUS);
		this.put("=",  TokenType.EQUAL);
		this.put("<>", TokenType.NEQUAL);
		this.put("<",  TokenType.LT);
		this.put("<=", TokenType.LTE);
		this.put(">",  TokenType.GT);
		this.put(">=", TokenType.GTE);
		this.put("*",  TokenType.MULT);
		this.put("/",  TokenType.DIV);
		this.put(":=", TokenType.ASSIGN);
		
		//KeyWords
		this.put("and",	      TokenType.AND);
		this.put("array",     TokenType.ARRAY);
		this.put("begin",     TokenType.BEGIN);
		this.put("div",       TokenType.INTDIV);
		this.put("do",	      TokenType.DO);
		this.put("else", 	  TokenType.ELSE);
		this.put("end", 	  TokenType.END);
		this.put("function",  TokenType.FUNCTION);
		this.put("if", 		  TokenType.IF);
		this.put("integer",   TokenType.INTEGER);
		this.put("mod", 	  TokenType.MOD);
		this.put("not", 	  TokenType.NOT);
		this.put("of", 		  TokenType.OF);
		this.put("or", 		  TokenType.OR);
		this.put("procedure", TokenType.PROCEDURE);
		this.put("program",   TokenType.PROGRAM);
		this.put("real", 	  TokenType.REAL);
		this.put("then", 	  TokenType.THEN);
		this.put("var", 	  TokenType.VAR);
		this.put("while", 	  TokenType.WHILE);
		this.put("write", 	  TokenType.WRITE);
		this.put("read", 	  TokenType.READ);
	}
}

