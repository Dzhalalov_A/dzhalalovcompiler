package scanner;
/**
 * This is scanner module of Pascal compiler.
 * This module was designed to produce a Sequence of Tokens out of source code
 * for following deciphering by Parser module.
 */

/* Declarations */

%%
%public                 /* makes output Scanner class public */
%class  	Scanner     /* Names the produced java file */
%function 	nextToken   /* Renames the yylex() function */
%type   	Token       /* Defines the return type of the scanning function */
%eofval{
  return null;
%eofval}
%line                   /* Enables line counting via yyline() method*/
%{
	LookUpTable LookUp = new LookUpTable(); //Creating an instance of LookUpTable for use with nextToken function
%}

/* Patterns */

int = [0-9]+

float = [0-9]+(\.)[0-9]+

exp = {float}+[E][+-]?{int}

word = [a-zA-Z]+([a-zA-Z] | [0-9])*

symbol = (\; | \, | \. | \: | \[ | \] | \( | \) | \+ | \- | \= | (<>) | \< | (<=) | \> | (>=) | \* | \/ | (:=))

comment = (\{)({int} | {float} |{exp} | {word} | {symbol} | {whitespace} | {other})*(\})

whitespace = [ \t\n\r]

other = .

%%

/* Lexical Rules */

{word}      {
				//Rule "word". Token Type ID or from LookUpTable.
				if(LookUp.get( yytext()) == null)
				{
					return (new Token(TokenType.ID, yytext()));
				}
				else
				{
					return (new Token(LookUp.get( yytext()), yytext()));
				}
			}
			
{int}     	{
                //Rule "int". Token Type INT.
				return(new Token(TokenType.INT, yytext()));
            }
			
{float}     {
                //Rule "float". Token Type FLOAT.
				return(new Token(TokenType.FLOAT, yytext()));
            }	
			
{exp}    	{
                //Rule exp. Token Type EXP.
				return(new Token(TokenType.EXP, yytext()));
            }				
			
{symbol}	{
                //Rule "symbol". Token Type from LookUpTable
				return(new Token(LookUp.get( yytext()), yytext()));
			}
			
{whitespace}  {  /* Ignore Whitespace */ 
                 // System.out.println("Whitespace found" + "\n");
              }

{comment}   {
                //Rule "comment". Comment section.
				System.out.print("Comment section " + yytext() + "\n");
			}

{other}    	{
                //Rule "other". Token Type INVALID.
				return(new Token(TokenType.INVALID, yytext()));
			}


           
