package scanner;

/**
 * Token class. A token object matches Enum token type with a input string
 * @author Artur Dzhalalov
 */

public class Token 
{
  
    public TokenType type;			//ENUM value of token type
    public String lexeme;			//Input string
    
    public Token(TokenType tt, String lx)
    {
		this.type = tt;				//Assigning ENUM TokenType from lookup table to type
        this.lexeme = lx;			//Assigning an input string to a lexeme        
    }

	/**
	 * Returns the string representation of current token.
	 * @return The String version of the operation token.
	 */
	@Override
	public String toString()
	{
		return("     Token: " + lexeme + "\n" + "Token Type: " + type + "\n");
	}

	//getters
	public TokenType getType(){return this.type;}
	public String 	 getLexeme(){return this.lexeme;}

	/**
	 * Override of the default "equals" function.
	 * Designed to test two Token objects for equality.
	 *
	 * @param o Object checked for equality against Token
	 * @return  Boolean True if objects are equal
	 */
	@Override
	public boolean equals (Object o)
	{
		//Test if actually token comes in
		if(!(o instanceof Token))
		{
			return false;
		}
		
		Token other = (Token) o; //Casting object to "true" Token type.
		
		boolean answer = true;   //Return value, initialized to true
		
		//Comparing string part of Token
		if( !this.lexeme.equals(other.lexeme) ) answer = false; //don't use '==' for string comparison
		
		//Comparing type part of Token
		if(this.type != other.type) answer = false;
		
		return answer;
	}
}