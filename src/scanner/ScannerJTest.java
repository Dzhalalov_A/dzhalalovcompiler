package scanner;
import org.junit.Test;
import static org.junit.Assert.*;
import java.io.StringReader;
import java.lang.*;
 
/**
 * Unit tests of the Scanner class, to be run with JUnit.
 * @author Artur Dzhalalov
 */
public class ScannerJTest {

     /**
     * Test of nextToken method, of Scanner class.
	 * Testing if TokenTypes match.
     */
    @Test
    public void test_nextToken() {
		System.out.println("test_nextToken");
		//Simplest Pascal program
		String input = "{ This is the simplest pascal program } program foo; begin end .";
        StringReader sr = new StringReader(input);
		Scanner s = new Scanner (sr);
		try
		{
			Token expected = new Token(TokenType.PROGRAM, "program");
			assertEquals(expected, s.nextToken());
			
			expected = new Token(TokenType.ID, "foo");
			assertEquals(expected, s.nextToken());
			
			expected = new Token(TokenType.SEMI, ";");
			assertEquals(expected, s.nextToken());
			
			expected = new Token(TokenType.BEGIN, "begin");
			assertEquals(expected, s.nextToken());
			
			expected = new Token(TokenType.END, "end");
			assertEquals(expected, s.nextToken());
			
			expected = new Token(TokenType.PERIOD, ".");
			assertEquals(expected, s.nextToken());
			
			System.out.println("nextToken() simple program test passed:)");
		}
		catch (Exception e) 
		{
			System.out.println("nextToken() simple program test failed!");
		}		
        
    }
}