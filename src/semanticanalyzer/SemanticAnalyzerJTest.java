package semanticanalyzer;
import com.sun.jdi.Value;
import parser.*;
import org.junit.jupiter.api.Test;
import scanner.TokenType;
import syntaxtree.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class SemanticAnalyzerJTest {
    /**
     * VariablesCheck test passes if all variables declared
     * Positive test: no output
     * Negative test: error message
     */
    @Test
    public void VariablesCheckPositiveTest(){
        Parser pr = new Parser("program foo;\n" +
                                    "var fee, fi, fo, fum: integer;\n" +
                                    "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                                    "begin\n" +
                                    "  fee := 4;\n" +
                                    "  fi := 5 * fee;\n" +
                                    "  fo := fi + 5;\n" +
                                    "  if fo < 13\n" +
                                    "    then\n" +
                                    "      fo := 4\n" +
                                    "    else\n" +
                                    "      fo := fo\n" +
                                    "  ;\n" +
                                    "  write( fo)\n" +
                                    "end\n" +
                                    ".\n", false);
        ProgramNode pn = pr.programRecognizer();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn);
    }

    /**
     * assignmentsTypeCheck test passes if all left and right parts of assignment match types
     * Positive test: no output
     * Negative test: error message
     */
    @Test
    public void assignmentsTypeCheckNegativeTest(){
        Parser pr = new Parser( "program foo;\n" +
                                    "var fee, fi, fo, fum: integer;\n" +
                                    "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                                    "begin\n" +
                                    "  fee := 4;\n" +
                                    "  fi := 5 * fee;\n" +
                                    "  fo := fi + 5;\n" +
                                    "  if fo < 13\n" +
                                    "    then\n" +
                                    "      fo := 4\n" +
                                    "    else\n" +
                                    "      fo := fo\n" +
                                    "  ;\n" +
                                    "  write( fo)\n" +
                                    "end\n" +
                                    ".\n", false);
        ProgramNode pn = pr.programRecognizer();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn);
        ArrayList<StatementNode> statements = new ArrayList<StatementNode>();   //creating collection of statements
        AssignmentStatementNode temp = new AssignmentStatementNode();           //initializing an instance of an assignment statement
        VariableNode fee = new VariableNode("fee");                       //Replacing existing var fee with same var but with different type
        fee.setType(ExpressionType.Integer);
        ValueNode num = new ValueNode("5.0");
        num.setType(ExpressionType.Real);
        temp.setLvalue(fee);                    //setting up left part of assignment statement
        temp.setExpression(num);                //setting up expression part of assignment statement
        statements.add(temp);                   //adding assignment statement node to the collection
        sa.assignmentsTypeCheck(statements);
    }

    /**
     * statementNodeVarCheck test passes if all variables in current node declared
     * Positive test: no output
     * Negative test: error message
     */
    @Test
    public void statementNodeVarCheckNegativeTest(){
        Parser pr = new Parser( "program foo;\n" +
                "var fee, fi, fo, fum: integer;\n" +
                "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                "begin\n" +
                "  fee := 4;\n" +
                "  fi := 5 * fee;\n" +
                "  fo := fi + 5;\n" +
                "  if fo < 13\n" +
                "    then\n" +
                "      fo := 4\n" +
                "    else\n" +
                "      fo := fo\n" +
                "  ;\n" +
                "  write( fo)\n" +
                "end\n" +
                ".\n", false);
        ProgramNode pn = pr.programRecognizer();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn);
        IOStatementNode io = new IOStatementNode();
        VariableNode var = new VariableNode("bar");
        io.setAttribute(var);
        sa.statementNodeVarCheck(io);
    }

    /**
     * expNodeVarCheck test passes if all variables in current node declared
     * Positive test: no output
     * Negative test: error message
     */
    @Test
    public void expNodeVarCheckPositiveTest(){
        Parser pr = new Parser( "program foo;\n" +
                                    "var fee, fi, fo, fum: integer;\n" +
                                    "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                                    "begin\n" +
                                    "  fee := 4;\n" +
                                    "  fi := 5 * fee;\n" +
                                    "  fo := fi + 5;\n" +
                                    "  if fo < 13\n" +
                                    "    then\n" +
                                    "      fo := 4\n" +
                                    "    else\n" +
                                    "      fo := fo\n" +
                                    "  ;\n" +
                                    "  write( fo)\n" +
                                    "end\n" +
                                    ".\n", false);
        ProgramNode pn = pr.programRecognizer();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn);
        OperationNode op = new OperationNode(TokenType.MULT);
        VariableNode var = new VariableNode("foo");
        op.setLeft(var);
        ValueNode num = new ValueNode("4.0");
        op.setRight(num);
        sa.opNodeTypeCheck(op);
    }

    /**
     * opNodeTypeCheck test passes if operation node type changes from null to a Real or Int
     */
    @Test
    public void opNodeTypeCheckPositiveTest(){
        Parser pr = new Parser( "program foo;\n" +
                                    "var fee, fi, fo, fum: integer;\n" +
                                    "function converter (x: integer): integer; var boo, foo: real; begin x := 74 end;\n" +
                                    "begin\n" +
                                    "  fee := 4;\n" +
                                    "  fi := 5 * fee;\n" +
                                    "  fo := fi + 5;\n" +
                                    "  if fo < 13\n" +
                                    "    then\n" +
                                    "      fo := 4\n" +
                                    "    else\n" +
                                    "      fo := fo\n" +
                                    "  ;\n" +
                                    "  write( fo)\n" +
                                    "end\n" +
                                    ".\n", false);
        ProgramNode pn = pr.programRecognizer();
        SemanticAnalyzer sa = new SemanticAnalyzer(pn);
        OperationNode op = new OperationNode(TokenType.MULT);
        String before = null;
        VariableNode var = new VariableNode("foo");
        op.setLeft(var);
        ValueNode num = new ValueNode("4.0");
        op.setRight(num);
        sa.opNodeTypeCheck(op);
        String after = op.getType().toString();
        assertNotEquals(before, after);
        System.out.println("before: " + before + "\nafter: " + after);
    }
}
