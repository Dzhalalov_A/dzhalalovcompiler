package semanticanalyzer;
import syntaxtree.*;

import java.util.ArrayList;

/**
 * This class performs semantic analysis of the parse tree build by Parser.
 */
public class SemanticAnalyzer {
    private ArrayList<VariableNode> DeclaredVars;
    public SemanticAnalyzer(ProgramNode pn){

        CompoundStatementNode comp = pn.getMain();
        DeclarationsNode declarations = pn.getDeclarations();
        this.DeclaredVars = declarations.getVariables();
        ArrayList<StatementNode> statements = comp.getStatements();

        VariablesCheck(statements);
        assignmentsTypeCheck(statements);
    }

    /**
     * This method checks if all the variables are declared
     * @param statements collection of statements
     */
    public void VariablesCheck(ArrayList<StatementNode> statements){
        //maybe add functionality to distinguish among main program and subprograms
        for(StatementNode st : statements){
            statementNodeVarCheck(st);
        }
    }

    /**
     * This method checks if all the variables are declared
     * @param statements collection of statements
     */
    public void assignmentsTypeCheck(ArrayList<StatementNode> statements){
        for(StatementNode st : statements){
            //ASSIGNMENT
            if(st instanceof AssignmentStatementNode){
                expNodeVarCheck(((AssignmentStatementNode) st).getLvalue());
                ExpressionType left = (((AssignmentStatementNode) st).getLvalue()).getType();
                if(((AssignmentStatementNode) st).getExpression() instanceof OperationNode){        //making sure operation node has expression type set
                    opNodeTypeCheck((OperationNode) ((AssignmentStatementNode) st).getExpression());
                }
                ExpressionType right = (((AssignmentStatementNode) st).getExpression()).getType();
                //Assignment type check
                if(left == ExpressionType.Integer && right == ExpressionType.Real){
                    System.out.println("Assignment of variable: " + ((AssignmentStatementNode) st).getLvalue() + " is invalid.");
                    System.out.println("Cannot assign type of " + right + " to type of " + left);
                }
            }
            //IF
            if(st instanceof IfStatementNode){
                ExpressionNode exp = ((IfStatementNode) st).getTest();
                opNodeTypeCheck((OperationNode) exp);
                StatementNode temp = ((IfStatementNode) st).getThenStatement();
                //testing then part assignment
                if(temp instanceof AssignmentStatementNode){
                    expNodeVarCheck(((AssignmentStatementNode) temp).getLvalue());
                    ExpressionType left = (((AssignmentStatementNode) temp).getLvalue()).getType();
                    if(((AssignmentStatementNode) temp).getExpression() instanceof OperationNode){        //making sure operation node has expression type set
                        opNodeTypeCheck((OperationNode) ((AssignmentStatementNode) temp).getExpression());
                    }
                    ExpressionType right = (((AssignmentStatementNode) temp).getExpression()).getType();
                    //Assignment type check
                    if(left == ExpressionType.Integer && right == ExpressionType.Real){
                        System.out.println("Assignment of variable\"" + ((AssignmentStatementNode) temp).getLvalue() + "\"in THEN part of IF is invalid.");
                        System.out.println("Cannot assign type of " + right + " to type of " + left);
                    }
                }
                //testing else part assignment
                temp = ((IfStatementNode) st).getElseStatement();
                if(temp instanceof AssignmentStatementNode){
                    expNodeVarCheck(((AssignmentStatementNode) temp).getLvalue());
                    ExpressionType left = (((AssignmentStatementNode) temp).getLvalue()).getType();
                    if(((AssignmentStatementNode) temp).getExpression() instanceof OperationNode){        //making sure operation node has expression type set
                        opNodeTypeCheck((OperationNode) ((AssignmentStatementNode) temp).getExpression());
                    }
                    ExpressionType right = (((AssignmentStatementNode) temp).getExpression()).getType();
                    //Assignment type check
                    if(left == ExpressionType.Integer && right == ExpressionType.Real){
                        System.out.println("Assignment of variable \"" + ((AssignmentStatementNode) temp).getLvalue() + "\" in ELSE part of IF is invalid.");
                        System.out.println("Cannot assign type of " + right + " to type of " + left);
                    }
                }
            }
            //WHILE
            if(st instanceof WhileStatementNode){
                ExpressionNode exp = ((WhileStatementNode) st).getTest();
                opNodeTypeCheck((OperationNode) exp);   //setting up the type of operation
                StatementNode temp = ((WhileStatementNode) st).getLoop();
                if(temp instanceof CompoundStatementNode){
                    assignmentsTypeCheck(((CompoundStatementNode) temp).getStatements());
                }
                else{
                    if(temp instanceof AssignmentStatementNode)
                        expNodeVarCheck(((AssignmentStatementNode) temp).getLvalue());
                    ExpressionType left = (((AssignmentStatementNode) temp).getLvalue()).getType();
                    if(((AssignmentStatementNode) temp).getExpression() instanceof OperationNode){        //making sure operation node has expression type set
                        opNodeTypeCheck((OperationNode) ((AssignmentStatementNode) temp).getExpression());
                    }
                    ExpressionType right = (((AssignmentStatementNode) temp).getExpression()).getType();
                    //Assignment type check
                    if(left != right){
                        System.out.println("Assignment of variable \"" + ((AssignmentStatementNode) temp).getLvalue() + "\" in invalid.");
                        System.out.println("Cannot assign type of " + right + " to type of " + left);
                    }
                }
            }
        }
    }
    /**
     * This method takes a statement node and determines if variable nodes connected to this node are declared
     * @param st An input statement node
     */
    public void statementNodeVarCheck(StatementNode st){
        //Assign
        if(st instanceof AssignmentStatementNode) {
            expNodeVarCheck(((AssignmentStatementNode) st).getLvalue());        //checking left part of assignment
            expNodeVarCheck(((AssignmentStatementNode) st).getExpression());    //checking expression part of assignment
        }
        //If Then Else
        else if(st instanceof IfStatementNode){
            ExpressionNode temp = ((IfStatementNode) st).getTest();
            expNodeVarCheck(temp);                                              //checking variables in test part
            statementNodeVarCheck(((IfStatementNode) st).getThenStatement());   //checking variables in then part
            statementNodeVarCheck(((IfStatementNode) st).getElseStatement());   //checking variables in else part
        }
        //While
        else if(st instanceof WhileStatementNode){
            ExpressionNode temp = ((WhileStatementNode) st).getTest();
            expNodeVarCheck(temp);                                      //checking variables in test part
            statementNodeVarCheck(((WhileStatementNode) st).getLoop()); //checking variables in loop's body part
        }
        //I/O
        else if(st instanceof IOStatementNode){
            ExpressionNode temp = ((IOStatementNode) st).getAttribute();
            expNodeVarCheck(temp);
        }
    }

    /**
     * This method takes an expression node and determines if variable nodes connected to this operation node are declared
     * @param exp An input expression node
     */
    public void expNodeVarCheck(ExpressionNode exp){
        if(exp instanceof OperationNode){
            expNodeVarCheck(((OperationNode) exp).getLeft());
            expNodeVarCheck(((OperationNode) exp).getRight());
            opNodeTypeCheck((OperationNode) exp);
        }else if(exp instanceof VariableNode){
            VariableNode temp = (VariableNode) exp;
            if(DeclaredVars.contains(temp)){ //Checking if variable declared, if not printing error msg
                int index = DeclaredVars.indexOf(temp);
                VariableNode tmp = DeclaredVars.get(index);
                temp.setType(tmp.getType()); //assigning type to a "field" variable node
            }
            else{
                System.out.println("Variable " + temp.getName() + " is not declared");
            }
        }
    }

    /**
     * This method determines the type of an operation node
     * @param op An input operation node
     */
    public void opNodeTypeCheck(OperationNode op){
        if(op.getLeft() instanceof OperationNode){          //complex operation node
            opNodeTypeCheck((OperationNode) op.getLeft());
        }
        if(op.getRight() instanceof OperationNode){    //complex operation node
            opNodeTypeCheck((OperationNode) op.getRight());
        }
        //simple operation node
        ExpressionType left = op.getLeft().getType();
        ExpressionType right = op.getRight().getType();
        if(left == ExpressionType.Real || right == ExpressionType.Real){
             op.setType(ExpressionType.Real);
        }
        else{
            op.setType(ExpressionType.Integer);
        }
    }
}
