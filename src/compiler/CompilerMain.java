package compiler;

import codegen.CodeGenerator;
import parser.Parser;
import semanticanalyzer.SemanticAnalyzer;
import syntaxtree.ProgramNode;

import java.io.*;
/**
 * This class is a facilitator of compiling process.
 */
public class CompilerMain {
    /**
     * Main method calls parser on the input file, it will report if parsing was successful or not, and it will create an output file.
     * @param args Command line arguments args[0] holds a filename of an input file.
     */
    public static void main (String[] args){

        //Creating new parser object
        Parser pr = new Parser(args[0], true);
        ProgramNode root = pr.programRecognizer();
        //Parser pr = new Parser("program foo; var fee: integer; begin write ( fee) end .", false);
        //System.out.println(args[0]);
        try{

            //Printing out ProgramNode
            //Formatting output files names
            String programTreeFileName = args[0].replace(".pas", "_ProgramNode.txt");
            String symbolTableFileName = args[0].replace(".pas", "_SymbolTable.txt");
            String asmFileName = args[0].replace(".pas", ".asm");

            try{
                // Writing SymbolTable to a file
                PrintWriter pw = new PrintWriter(symbolTableFileName);
                pw.print(pr.st.toString());
                pw.close();
                //Semantic analyzer
                SemanticAnalyzer sa = new SemanticAnalyzer(root);

                // Writing ProgramNode to a file
                pw = new PrintWriter(programTreeFileName);
                pw.print(root.indentedToString(0));
                pw.close();


                // Writing Asm code to a file
                pw = new PrintWriter(asmFileName);
                CodeGenerator cg = new CodeGenerator();
                pw.print(cg.AsmCodeGenerator(root));
                pw.close();
            }
            catch (Exception e){
                System.out.print("Error! " + e);
            }
            //System.out.println("Parsed! :)");
        }
        catch (Exception e){
            System.out.println("Error! " + e);
            System.out.println("Didn't parse :(");
        }

    }
}
