package codegen;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import parser.Parser;
import syntaxtree.ProgramNode;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import static org.junit.jupiter.api.Assertions.*;

class CodeGeneratorTest {
    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * with assignment statements
     */
    @Test
    void AssignmentTestIntToInt() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var fee, fi, fo: integer;\n" +
                            "begin\n" +
                            "fi := 34;\n" +
                            "fo := fo * 10;\n" +
                            "fee := 20 * fi + fo\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "fee:      .word 0\n" +
                            "fi:       .word 0\n" +
                            "fo:       .word 0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\taddi   $t0, $zero, 34\n" +
                            "\tsw     $t0, fi\n" +
                            "\tlw     $t0, fo\n" +
                            "\taddi   $t1, $zero, 10\n" +
                            "\tmul    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fo\n" +
                            "\taddi   $t1, $zero, 20\n" +
                            "\tlw     $t2, fi\n" +
                            "\tmul    $f1, $t1, $t2\n" +
                            "\tlw     $t0, fo\n" +
                            "\tadd    $f0, $f1, $t0\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fee\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * with assignment statements
     */
    @Test
    void AssignmentTestFloatToFloat() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var free, fri: real;\n" +
                            "begin\n" +
                            "free := 1.5;\n" +
                            "fri := free / 0.5\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "free:     .float 0.0\n" +
                            "fri:      .float 0.0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\tli.s   $f1, 1.5\n" +
                            "\tmov.s  $f0, $f1\n" +
                            "\tswc1   $f0, free\n" +
                            "\tl.s    $f1, free\n" +
                            "\tli.s   $f3, 0.5\n" +
                            "\tmov.s  $f2, $f3\n" +
                            "\tdiv.s  $f0, $f1, $f2\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fri\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * with assignment statements
     */
    @Test
    void AssignmentTestFloatToInt() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var free, fri: real;\n" +
                            "begin\n" +
                            "free := 5;\n" +
                            "fri := free / 0.5\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "free:     .float 0.0\n" +
                            "fri:      .float 0.0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\taddi   $t0, $zero, 5\n" +
                            "\tmtc1   $t0, free\n" +
                            "\tcvt.s.wfree, free\n" +
                            "\tl.s    $f1, free\n" +
                            "\tli.s   $f3, 0.5\n" +
                            "\tmov.s  $f2, $f3\n" +
                            "\tdiv.s  $f0, $f1, $f2\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fri\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * containing IF statement
     */
    @Test
    void asmCodeGeneratorTestIfThenElseInt() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var fee, fi, fo: integer;\n" +
                            "begin\n" +
                            "fi := 34;\n" +
                            "fo := fi * 10;\n" +
                            "if fi > fo\n" +
                            "    then fee := 1\n" +
                            "    else fee := 0\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "fee:      .word 0\n" +
                            "fi:       .word 0\n" +
                            "fo:       .word 0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\taddi   $t0, $zero, 34\n" +
                            "\tsw     $t0, fi\n" +
                            "\tlw     $t0, fi\n" +
                            "\taddi   $t1, $zero, 10\n" +
                            "\tmul    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fo\n" +
                            "\tlw     $t0, fi\n" +
                            "\tlw     $t1, fo\n" +
                            "\tsgt    $f0, $t0, $t1\n" +
                            "\tbc1t   ifTrue_0\n" +
                            "\taddi   $t0, $zero, 0\n" +
                            "\tsw     $t0, fee\n" +
                            "\tj      ifDone_0\n" +
                            "ifTrue_0:\n" +
                            "\taddi   $t0, $zero, 1\n" +
                            "\tsw     $t0, fee\n" +
                            "ifDone_0:\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * containing IF statement
     */
    @Test
    void asmCodeGeneratorTestIfThenElseFloat() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var fee: integer;\n" +
                            "var free, fri : real;" +
                            "begin\n" +
                            "free := 2.67;\n" +
                            "fri := 0.1;\n" +
                            "if free < fri\n" +
                            "    then fee := 1\n" +
                            "    else fee := 0\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "fee:      .word 0\n" +
                            "free:     .float 0.0\n" +
                            "fri:      .float 0.0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\tli.s   $f1, 2.67\n" +
                            "\tmov.s  $f0, $f1\n" +
                            "\tswc1   $f0, free\n" +
                            "\tli.s   $f1, 0.1\n" +
                            "\tmov.s  $f0, $f1\n" +
                            "\tswc1   $f0, fri\n" +
                            "\tl.s    $f0, free\n" +
                            "\tl.s    $f1, fri\n" +
                            "\tc.lt.s $f0, $f1\n" +
                            "\tbc1t   ifTrue_0\n" +
                            "\taddi   $t0, $zero, 0\n" +
                            "\tsw     $t0, fee\n" +
                            "\tj      ifDone_0\n" +
                            "ifTrue_0:\n" +
                            "\taddi   $t0, $zero, 1\n" +
                            "\tsw     $t0, fee\n" +
                            "ifDone_0:\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * containing WHILE statement
     */
    @Test
    void asmCodeGeneratorTestWhileInt() {
        CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var fee, fi, fo: integer;\n" +
                            "begin\n" +
                            "fi := 34;\n" +
                            "fo := fi * 10;\n" +
                            "while fo > 0\n" +
                            "    do\n" +
                            "        begin\n" +
                            "            fo := fo - 1;\n" +
                            "            fi := fi * 10\n" +
                            "        end\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "fee:      .word 0\n" +
                            "fi:       .word 0\n" +
                            "fo:       .word 0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\taddi   $t0, $zero, 34\n" +
                            "\tsw     $t0, fi\n" +
                            "\tlw     $t0, fi\n" +
                            "\taddi   $t1, $zero, 10\n" +
                            "\tmul    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fo\n" +
                            "\tWhileTest_0:\n" +
                            "\tlw     $t0, fo\n" +
                            "\taddi   $t1, $zero, 0\n" +
                            "\tsgt    $f0, $t0, $t1\n" +
                            "\tbc1f   WhileDone_0\n" +
                            "\tlw     $t0, fo\n" +
                            "\taddi   $t1, $zero, 1\n" +
                            "\tsub    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fo\n" +
                            "\tlw     $t0, fi\n" +
                            "\taddi   $t1, $zero, 10\n" +
                            "\tmul    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fi\n" +
                            "\tj      WhileTest_0\n" +
                            "\tWhileDone_0:\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }

    /**
     * This method tests if CodeGenerator returns correct asm string for given pascal program
     * containing WHILE statement
     */
    @Test
    void asmCodeGeneratorTestWhileFloat() {CodeGenerator codeGen = new CodeGenerator();
        String program =    "program foo;\n" +
                            "var fee, fi, fo: integer;\n" +
                            "var free: real;\n" +
                            "begin\n" +
                            "free := 0.75;\n" +
                            "fi := 0;\n" +
                            "while free > 0\n" +
                            "    do\n" +
                            "        begin\n" +
                            "            free := free - 0.1;\n" +
                            "            fi := fi + 1\n" +
                            "        end\n" +
                            "end\n" +
                            ".";
        String expected =   "#Asm code for pascal program: foo\n" +
                            "\t.data\n" +
                            "fee:      .word 0\n" +
                            "fi:       .word 0\n" +
                            "fo:       .word 0\n" +
                            "free:     .float 0.0\n" +
                            "\t.text\n" +
                            "main:\n" +
                            "\tli.s   $f1, 0.75\n" +
                            "\tmov.s  $f0, $f1\n" +
                            "\tswc1   $f0, free\n" +
                            "\taddi   $t0, $zero, 0\n" +
                            "\tsw     $t0, fi\n" +
                            "\tWhileTest_0:\n" +
                            "\tl.s    $f0, free\n" +
                            "\taddi   $t0, $zero, 0\n" +
                            "\tmtc1   $t0, $f1\n" +
                            "\tcvt.s.w$f1, $f1\n" +
                            "\tc.lt.s $f1, $f0\n" +
                            "\tbc1f   WhileDone_0\n" +
                            "\tl.s    $f1, free\n" +
                            "\tli.s   $f3, 0.1\n" +
                            "\tmov.s  $f2, $f3\n" +
                            "\tsub.s  $f0, $f1, $f2\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, free\n" +
                            "\tlw     $t0, fi\n" +
                            "\taddi   $t1, $zero, 1\n" +
                            "\tadd    $f0, $t0, $t1\n" +
                            "\tmfc1   $t0, $f0\n" +
                            "\tsw     $t0, fi\n" +
                            "\tj      WhileTest_0\n" +
                            "\tWhileDone_0:\n" +
                            "\tjr     $ra";
        Parser pr = new Parser(program, false);
        ProgramNode root = pr.programRecognizer();
        codeGen.AsmCodeGenerator(root);
        // Writing Asm code to a file
        CodeGenerator cg = new CodeGenerator();
        String actual = cg.AsmCodeGenerator(root);
        Assert.assertEquals(expected, actual);
    }
}