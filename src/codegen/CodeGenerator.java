package codegen;

import scanner.TokenType;
import syntaxtree.*;

/**
 * This class generates MIPS Assembly code for Pascal program.
 */
public class CodeGenerator{
    private StringBuilder out = new StringBuilder();
    private int tIndex = 0;     //index of available t register
    private int fIndex = 0;     //index of available f register
    private int ifIndex = 0;    //index of available IF label
    private int whileIndex = 0; //index of available While loop labels
    /**
     * Constructor of CodeGen object
     * @param pn Program Node of a syntax tree corresponding to a Pascal program
     */
    public String AsmCodeGenerator(ProgramNode pn){
        DeclarationsNode data = pn.getDeclarations();   //getting variables list
        CompoundStatementNode main = pn.getMain();      //getting statements list
        out.append(String.format("#Asm code for pascal program: %s\n", pn.getName()));
        writeDotData(data);                             //adding .data section to asm output code
        writeDotText();                                 //adding .text section to asm output code
        writeMain(main);                                //adding .main section to asm output code
        //sub program. place holder.
        out.append(String.format("\t%-7s%s", "jr", "$ra")); //Done.
        return this.out.toString();
    }
    /**
     * Adds code for .data section
     * @param data Collection of Variable nodes
     */
    public void writeDotData(DeclarationsNode data){
        out.append("\t.data\n");
        for(VariableNode var : data.getVariables()){
            if(var.getType() == ExpressionType.Integer){
                out.append(String.format("%-10s%s", var.getName() + ":", ".word 0\n")); //initializing var to 0
            }
            else if(var.getType() == ExpressionType.Real){
                out.append(String.format("%-10s%s", var.getName() + ":", ".float 0.0\n")); //initializing var to 0.0
            }
            else{
                System.out.println("Varible " + var.getName() + " has invalid type " + var.getType());
            }

        }
    }

    /**
     * Adds code for .text section
     */
    public void writeDotText(){
        out.append("\t.text\n");
    }

    /**
     * Adds code for .main section
     */
    public void writeMain(CompoundStatementNode main){
        out.append("main:\n");
        for(StatementNode st : main.getStatements()){
            writeStatement(st);
        }
    }

    /**
     * Determines type of a statement and calls corresponding helper function
     * @param st Statement node
     */
    public void writeStatement(StatementNode st){
        //Assignment
        if(st instanceof AssignmentStatementNode){
            ExpressionNode exp = ((AssignmentStatementNode) st).getExpression();
            String src;
            String answer;
            Boolean tDecrement = false;
            Boolean fDecrement = false;
            //Getting type of exp to determine what type of register is needed for the result
            if(exp.getType() == ExpressionType.Integer){
                src = "$t"+tIndex++;
                tDecrement = true;
            }
            else{
                src = "$f"+fIndex++;
                fDecrement = true;
            }
            writeExpression(exp, src);                                          //asm code for exp part of assignment
            String dst = ((AssignmentStatementNode) st).getLvalue().getName();  //destination register
            if(((AssignmentStatementNode) st).getLvalue().getType() == ExpressionType.Integer
                                                   && exp.getType() == ExpressionType.Integer){ //int := int
                answer = String.format("\t%-7s%s, %s\n", "sw", src, dst);        //sw source_reg, destination_reg
            }
            else if (((AssignmentStatementNode) st).getLvalue().getType() == ExpressionType.Real
                                                         && exp.getType() == ExpressionType.Integer){ //float := int
                answer = String.format("\t%-7s%s, %s\n", "mtc1", src, "$f"+fIndex );
                answer += String.format("\t%-7s%s, %s\n", "cvt.s.w", "$f"+fIndex, "$f"+fIndex); //convert from int to single (a.k.a float)
                answer += String.format("\t%-7s%s, %s\n", "swc1", "$f"+fIndex, dst);
            }
            else if(((AssignmentStatementNode) st).getLvalue().getType() == ExpressionType.Real
                    && exp.getType() == ExpressionType.Real){ //float := float
                answer = String.format("\t%-7s%s, %s\n", "swc1", src, dst);
            }
            else{   // int := float
                answer = String.format("\t%-7s%s, %s\n", "mfc1", "$t" + tIndex, src); //Hmmm, loading float into int.
                answer += String.format("\t%-7s%s, %s\n", "sw", "$t" + tIndex, dst);
            }
            out.append(answer);
            if(tDecrement){
                tIndex--;
            }
            if(fDecrement){
                fIndex--;
            }

        }

        //Procedure (place holder)

        //Compound
        if(st instanceof CompoundStatementNode){
            for(StatementNode stNode : ((CompoundStatementNode) st).getStatements()){
                writeStatement(stNode);
            }
        }
        //IfThenElse
        if(st instanceof IfStatementNode){
            if(((IfStatementNode) st).getTest().getType() == ExpressionType.Integer){                           //checking type of expression
                writeExpression(((IfStatementNode) st).getTest(), "$t"+tIndex);                          //comparison for T registers
                out.append(String.format("\t%-7s%s, %s, %s\n", "beq", "$t"+tIndex, "1", "ifTrue_"+ifIndex));   //branch for T registers
            }
            else{
                writeExpression(((IfStatementNode) st).getTest(), "$f"+fIndex);                       //comparison for F registers
                OperationNode op = (OperationNode) ((IfStatementNode) st).getTest();
                if(((OperationNode) ((IfStatementNode) st).getTest()).getOperation() == TokenType.NEQUAL){  //not equals is a special case
                    out.append(String.format("\t%-7s%s\n", "bc1f", "ifTrue_"+ifIndex));                     //because there is no not equal comparison
                }                                                                                           //ne comp replaced with eq comp and branch on false
                else{
                    out.append(String.format("\t%-7s%s\n", "bc1t", "ifTrue_"+ifIndex));                     //branch for F registers (regular case)
                }

            }
            writeStatement(((IfStatementNode) st).getElseStatement());
            out.append(String.format("\t%-7s%s\n", "j", "ifDone_"+ifIndex));
            out.append(String.format("%s\n", "ifTrue_"+ifIndex + ":"));
            writeStatement(((IfStatementNode) st).getThenStatement());
            out.append(String.format("%s\n", "ifDone_"+ifIndex+":"));
            ifIndex++;
        }
        if(st instanceof WhileStatementNode){
            out.append(String.format("\t%s\n", "WhileTest_" + whileIndex + ":"));
            if(((WhileStatementNode) st).getTest().getType() == ExpressionType.Integer){
                writeExpression(((WhileStatementNode) st).getTest(), "$t"+tIndex);                               //comparison for T registers
                out.append(String.format("\t%-7s%s, %s, %s\n", "bne", "$t"+tIndex, "1", "WhileDone_" + whileIndex)); //loop's test
            }
            else{
                writeExpression(((WhileStatementNode) st).getTest(), "$f"+fIndex); //comparison for F registers
                OperationNode op = (OperationNode) ((WhileStatementNode) st).getTest();
                if(((OperationNode) ((WhileStatementNode) st).getTest()).getOperation() == TokenType.NEQUAL){
                    out.append(String.format("\t%-7s%s\n", "bc1t", "WhileDone_" + whileIndex)); //reversed loop's test spacial case for <>
                }
                else{
                    out.append(String.format("\t%-7s%s\n", "bc1f", "WhileDone_" + whileIndex)); //loop's test
                }

            }

            writeStatement(((WhileStatementNode) st).getLoop());
            out.append(String.format("\t%-7s%s\n", "j", "WhileTest_"+whileIndex));  //back to loop's test
            out.append(String.format("\t%s\n", "WhileDone_" + whileIndex + ":"));
        }
        if(st instanceof IOStatementNode){
            String ioType = ((IOStatementNode) st).getOperation();
            if(ioType == "read"){
                String varReg = ((IOStatementNode) st).getVarName();
                writeInputPrompt();
                if(((IOStatementNode) st).getAttribute().getType() == ExpressionType.Integer){
                    out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "5"));       //li $v0, 5
                    out.append("\tsyscall\n");                                           //syscall
                    out.append(String.format("\t%-7s%s, %s\n", "sw", "$v0", varReg));  //move $t0, $v0
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "6"));       //li $v0, 6 result goes in f0
                    out.append("\tsyscall\n");                                           //syscall
                    out.append(String.format("\t%-7s%s, %s\n", "swc1", "$f0", varReg)); //mov.s $f#, $f0
                }
            }
            else if(ioType == "write"){
                ExpressionNode output = ((IOStatementNode) st).getAttribute();
                if(output.getType() == ExpressionType.Integer){
                    writeExpression(output, "$t"+tIndex);
                    out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "1"));           //li  $v0, 1  #service 1 is print integer
                    out.append(String.format("\t%-7s%s, %s\n", "move", "$a0", "$t"+tIndex)); //move $a0, $t#
                    out.append("\tsyscall\n");                                               //syscall
                }
                else{
                    writeExpression(output, "$f"+fIndex);
                    out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "2"));            //li  $v0, 1  #service 2 is print float
                    out.append(String.format("\t%-7s%s, %s\n", "mov.s", "$f12", "$f"+fIndex));//mov.s $f12, $f#
                    out.append("\tsyscall\n");                                                //syscall
                }
                writeNewLine();
            }
            else{
                System.out.println("Invalid input function!!!");
            }
        }


    }

    /**
     * Determines type of an expression and calls corresponding helper function
     * @param exp Expression node
     * @param answer register to hold the result of expression
     */
    public void writeExpression(ExpressionNode exp, String answer){
        if(exp instanceof OperationNode){
            writeExpression((OperationNode)exp, answer);
        }
        if(exp instanceof VariableNode){
            writeExpression((VariableNode) exp, answer);
        }
        if(exp instanceof ValueNode){
            writeExpression((ValueNode) exp, answer);
        }
    }

    /**
     * Adds code for an Operation node.
     * First it will produce code for child nodes and return result registers of child nodes in operandOne (left child) and operandTwo (right child).
     * Then it will produce code for the operation itself.
     * @param op Expression node.
     * @param answer register to hold the result of expression.
     */
    public void writeExpression(OperationNode op, String answer){
        ExpressionNode left = op.getLeft();
        ExpressionNode right = op.getRight();
        String operandOne;
        String operandTwo;
        boolean leftTDecrement = false;
        boolean leftFDecrement = false;
        boolean rightTDecrement = false;
        boolean rightFDecrement = false;
        boolean opInt = false;
        if(left.getType() == ExpressionType.Integer){
            operandOne = "$t" + tIndex++;
            leftTDecrement = true;
        }
        else{
            operandOne = "$f" + fIndex++;
            leftFDecrement = true;
        }

        if(right.getType() == ExpressionType.Integer){
            operandTwo = "$t" + tIndex++;
            rightTDecrement = true;
        }
        else{
            operandTwo = "$f" + fIndex++;
            rightFDecrement = true;
        }

        writeExpression(left, operandOne);
        writeExpression(right, operandTwo);
        //Setting up operation's data type and moving int registers if needed
        if(left.getType() == ExpressionType.Integer && right.getType() == ExpressionType.Real){         //moving int reg to float if one operand is real and other is int
            out.append(String.format("\t%-7s%s, %s\n", "mtc1", operandOne, "$f" + fIndex));
            out.append(String.format("\t%-7s%s, %s\n", "cvt.s.w", "$f" + fIndex, "$f" + fIndex));
            operandOne = "$f" + fIndex;
        }
        else if(left.getType() == ExpressionType.Real && right.getType() == ExpressionType.Integer){    //moving int reg to float if one operand is real and other is int
            out.append(String.format("\t%-7s%s, %s\n", "mtc1", operandTwo, "$f" + fIndex));
            out.append(String.format("\t%-7s%s, %s\n", "cvt.s.w", "$f" + fIndex, "$f" + fIndex));
            operandTwo = "$f" + fIndex;
        }
        else if(left.getType() == ExpressionType.Real && right.getType() == ExpressionType.Real){
        }
        else{
            opInt = true;
        }
        TokenType opType = op.getOperation();
        switch (opType){
            case PLUS:
                if(opInt){
                    //add	$t0,$t1,$t2	#  $t0 = $t1 + $t2;
                    out.append(String.format("\t%-7s%s, %s, %s\n", "add", answer, operandOne, operandTwo));
                }
                else{
                    //add.s	$f0,$f1,$f2	#  $f0 = $f1 + $f2;
                    out.append(String.format("\t%-7s%s, %s, %s\n", "add.s", answer, operandOne, operandTwo));
                }
                break;
            case MINUS:
                if(opInt){
                    //sub	$t1,$t2,$t3	#  $t1 = $t2 - $t4
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sub", answer, operandOne, operandTwo));
                }
                else{
                    //sub.s	$f1,$f2,$f3	#  $f1 = $f2 - $f4
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sub.s", answer, operandOne, operandTwo));
                }
                break;
            case MULT:
                if(opInt){
                    //mul $t0, $t1, $t2 # $t0 gets Lo
                    out.append(String.format("\t%-7s%s, %s, %s\n", "mul", answer, operandOne, operandTwo));
                }
                else{
                    //mul.s $f0, $f1, $f2
                    out.append(String.format("\t%-7s%s, %s, %s\n", "mul.s", answer, operandOne, operandTwo));
                }
                break;
            case DIV:
                if(opInt){
                    //div rs, rt # Lo = rs / rt quotient goes to Lo, remainder to Hi
                    out.append(String.format("\t%-7s%s, %s, %s\n", "div", answer, operandOne, operandTwo));
                }
                else{
                    //div.s rs, rt # $f0 := $f1 / $f2
                    out.append(String.format("\t%-7s%s, %s, %s\n", "div.s", answer, operandOne, operandTwo));
                }
                break;
            case LT:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "slt", answer, operandOne, operandTwo));
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.lt.s", operandOne, operandTwo));
                }

                break;
            case LTE:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sle", answer, operandOne, operandTwo));
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.le.s", operandOne, operandTwo));
                }
                break;
            case GT:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sgt", answer, operandOne, operandTwo));
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.lt.s", operandTwo, operandOne));
                }
                break;
            case GTE:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sle", answer, operandTwo, operandOne)); //sge didn't work idk why
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.le.s", operandTwo, operandOne));
                }
                break;
            case EQUAL:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "seq", answer, operandOne, operandTwo));
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.eq.s", operandOne, operandTwo));
                }
                break;
            case NEQUAL:
                if(opInt){
                    out.append(String.format("\t%-7s%s, %s, %s\n", "sne", answer, operandOne, operandTwo));
                }
                else{
                    out.append(String.format("\t%-7s%s, %s\n", "c.eq.s", operandOne, operandTwo));
                }
                break;
        }
        //resetting registers' counts
        if(leftTDecrement){
            tIndex--;
        }
        if(leftFDecrement){
            fIndex--;
        }
        if(rightTDecrement){
            tIndex--;
        }
        if(rightFDecrement){
            fIndex--;
        }
    }

    /**
     * Adds code for a Value node.
     * @param vn Value node.
     * @param answer register to hold the result of expression.
     */
    public void writeExpression(ValueNode vn, String answer){
        String value = vn.getAttribute();
        if(vn.getType() == ExpressionType.Integer){
            out.append(String.format("\t%-7s%s, %s, %s\n", "addi", answer, "$zero", value)); //integer
        }
        else{
            out.append(String.format("\t%-7s%s, %s\n", "li.s", "$f" + fIndex, value)); //loading 0 into f reg, because there is no $zero for floats
            out.append(String.format("\t%-7s%s, %s\n", "mov.s", answer, "$f" + fIndex));
        }
    }

    /**
     * Adds code for a Variable node.
     * @param var Variable node.
     * @param answer register to hold the result of expression.
     */
    public void writeExpression(VariableNode var, String answer){
        String name = var.getName();
        if(var.getType() == ExpressionType.Integer){
            out.append(String.format("\t%-7s%s, %s\n", "lw", answer, name));
        }
        else{
            out.append(String.format("\t%-7s%s, %s\n", "l.s", answer, name));
        }

    }

    /**
     * Writes asm code that prints out new line char
     */
    public void writeNewLine(){
        out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "11"));         //li  $v0, 11  #service 11 is print char
        out.append(String.format("\t%-7s%s, %s\n", "la", "$a0", "10"));         //move $a0, $t#
        out.append("\tsyscall\n");
    }

    /**
     * Writes asm code that prints out input prompt for user
     */
    public void writeInputPrompt(){
        out.append(String.format("\t%-7s%s, %s\n", "li", "$v0", "11"));         //li  $v0, 11  #service 11 is print char
        out.append(String.format("\t%-7s%s, %s\n", "la", "$a0", "62"));         //move $a0, $t#
        out.append("\tsyscall\n");
    }
}