package syntaxtree;
/**
 * Represents an while do statement in Pascal.
 * An while do statement includes a boolean expression loop's test and statement loop's body .
 */
public class  WhileStatementNode extends StatementNode {
    private ExpressionNode test;
    private StatementNode loop;

    public ExpressionNode getTest() {
        return test;
    }

    public void setTest(ExpressionNode test) {
        this.test = test;
    }

    public StatementNode getLoop() {
        return loop;
    }

    public void setLoop(StatementNode loop) {
        this.loop = loop;
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "While\n";
        answer += this.test.indentedToString( level + 1);
        answer += this.loop.indentedToString( level + 1);
        return answer;
    }
}