package syntaxtree;

/**
 * General representation of any expression.
 * @author erik
 */
public abstract class ExpressionNode extends SyntaxTreeNode {
    ExpressionType type;
    /**
     * Returns the attribute of this node.
     * @return The type of this ValueNode.
     */
    public ExpressionType getType() { return( this.type);}

    /**
     * Sets the type of this node.
     * @return The ExpressionType.
     */
    public void setType(ExpressionType type) { this.type = type;}

}
