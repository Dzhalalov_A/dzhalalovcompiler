package syntaxtree;

public class ExpressionListNode extends ExpressionNode {
    /** The operator of this operation. */
    private ExpressionNode[] expressions;

    static private int index = 0;

    /**
     * Creates an ExpressionList node.
     */
    public ExpressionListNode (){}

    //Getters
    public ExpressionNode[] getList() {
        return expressions;
    }

    //Setters
    public void addExpressionNode(ExpressionNode child) {
        this.expressions[index] = child;
        index++;
    }

    @Override
    public String toString() {
        return expressions.toString();
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Expression List: \n";
        for(int i = 0; i < index; i++){
            answer += expressions[i].indentedToString(level + 1);
        }
        return( answer);
    }
}
