package syntaxtree;

import org.junit.jupiter.api.Test;
import scanner.TokenType;
import static org.junit.Assert.*;
import parser.Parser;

/**
 Unit tests of the SyntaxTree class, to be run with JUnit.
 */
public class SyntaxTreeJTest {

    /**
     * ProgramNode test
     */
    @Test
    public void ProgramNodeReturnTest(){
        //SyntaxTree for simplest.pas
        String expected =   "Program: foo" + "\n" +
                            "|-- Declarations" + "\n" +
                            "|-- SubProgram Declarations" + "\n" +
                            "|-- Compound Statement" + "\n";

        //Initializing parser for simplest.pas
        Parser pr = new Parser("program foo; begin end .\n", false);
        //Converting ProgramNode received from parser
        String actual = pr.programRecognizer().indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

    /**
     * ProgramNode test
     */
    @Test
    public void ProgramNodeFullTest(){
        //SyntaxTree for bitcoin example
        String expected =   "Program: sample" + "\n" +
                            "|-- Declarations" + "\n" +
                            "|-- --- Name: dollars" + "\n" +
                            "|-- --- Name: yen" + "\n" +
                            "|-- --- Name: bitcoins" + "\n" +
                            "|-- SubProgram Declarations" + "\n" +
                            "|-- Compound Statement" + "\n" +
                            "|-- --- Assignment" + "\n" +
                            "|-- --- --- Name: dollars" + "\n" +
                            "|-- --- --- Value: 1000000" + "\n" +
                            "|-- --- Assignment" + "\n" +
                            "|-- --- --- Name: yen" + "\n" +
                            "|-- --- --- Operation: MULT" + "\n" +
                            "|-- --- --- --- Name: dollars" + "\n" +
                            "|-- --- --- --- Value: 102" + "\n" +
                            "|-- --- Assignment" + "\n" +
                            "|-- --- --- Name: bitcoins" + "\n" +
                            "|-- --- --- Operation: DIV" + "\n" +
                            "|-- --- --- --- Name: dollars" + "\n" +
                            "|-- --- --- --- Value: 400" + "\n";

        //Declaring main nodes
        ProgramNode pn = new ProgramNode("sample");
        DeclarationsNode vars = new DeclarationsNode();
        SubProgramDeclarationsNode spd = new SubProgramDeclarationsNode();
        CompoundStatementNode cs = new CompoundStatementNode();

        //Setting values of Root's children nodes
        pn.setDeclarations(vars);
        pn.setFunctions(spd);
        pn.setMain(cs);

        //Adding variable nodes to Declarations
        VariableNode dollars = new VariableNode("dollars");
        vars.addVariable(dollars);
        VariableNode yen = new VariableNode("yen");
        vars.addVariable(yen);
        VariableNode bitcoins = new VariableNode("bitcoins");
        vars.addVariable(bitcoins);

        //Adding nodes to SubProgramDeclarations
        /*Nothing to add for bitcoin example*/

        //Adding nodes to Compound Statements
        //dollars := 1000000;
        AssignmentStatementNode assignDollars = new AssignmentStatementNode();
        assignDollars.setLvalue(dollars);
        ValueNode dollarAmount = new ValueNode("1000000");
        assignDollars.setExpression(dollarAmount);
        cs.addStatement(assignDollars);

        //yen := dollars * 102;
        AssignmentStatementNode assignYen = new AssignmentStatementNode();
        assignYen.setLvalue(yen);
        OperationNode mulOp = new OperationNode(TokenType.MULT);
        assignYen.setExpression(mulOp);
        mulOp.setLeft(dollars);
        ValueNode yenRate = new ValueNode("102");
        mulOp.setRight(yenRate);
        cs.addStatement(assignYen);

        //bitcoin := dollars / 400;
        AssignmentStatementNode assignBitcoins = new AssignmentStatementNode();
        assignBitcoins.setLvalue(bitcoins);
        OperationNode divOp = new OperationNode(TokenType.DIV);
        assignBitcoins.setExpression(divOp);
        divOp.setLeft(dollars);
        ValueNode bitcoinRate = new ValueNode("400");
        divOp.setRight(bitcoinRate);
        cs.addStatement(assignBitcoins);

        //Setting actual string value
        String actual = pn.indentedToString(0);

        //Comparing strings
        assertEquals(expected, actual);
    }

}