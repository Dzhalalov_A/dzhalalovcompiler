package syntaxtree;
import scanner.TokenType;

public class ProcedureNode extends StatementNode {
    /** The operator of this operation. */
    private ExpressionListNode expressionList;

    /** The name of procedure. */
    private String name;

    /**
     * Creates an unary operation node given an unary operator token.
     * @param attr The name of the procedure.
     */
    public ProcedureNode ( String attr) {
        this.name = attr;
    }

    //Getters
    public ExpressionListNode getExpressions() {
        return expressionList;
    }

    public String getName() {
        return name;
    }

    //Setters
    public void setExpressions(ExpressionListNode child) {
        this.expressionList = child;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return expressionList.toString();
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Procedure: " + this.name + "\n";
        answer += expressionList.indentedToString(level + 1);
        return( answer);
    }
}