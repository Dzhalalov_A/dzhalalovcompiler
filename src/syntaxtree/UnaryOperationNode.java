package syntaxtree;
import scanner.TokenType;
/**
 * Represents an unary operation on any expression.
 * @author Artur Dzhalalov
 */
public class UnaryOperationNode extends ExpressionNode {
    /** The operator of this operation. */
    private ExpressionNode operand;

    /** The kind of operation. */
    private TokenType operator;

    /**
     * Creates an unary operation node given an unary operator token.
     * @param op The token representing this node's math operation.
     */
    public UnaryOperationNode ( TokenType op) {
        this.operator = op;
    }

    //Getters
    public ExpressionNode getOperand() {
        return operand;
    }

    public TokenType getOperator() {
        return operator;
    }

    //Setters
    public void setOperand(ExpressionNode operand) {
        this.operand = operand;
    }

    public void setOperator(TokenType operator) {
        this.operator = operator;
    }

    @Override
    public String toString() {
        return operand.toString();
    }

    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation(level);
        answer += "Operation: " + this.operator + "\n";
        answer += operand.indentedToString(level + 1);
        return( answer);
    }
}
