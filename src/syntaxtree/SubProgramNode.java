package syntaxtree;

public class SubProgramNode extends SyntaxTreeNode{

    private String name;
    private ParametersNode parameters;
    private DeclarationsNode variables;
    private SubProgramDeclarationsNode functions;
    private CompoundStatementNode main;

    /**
     * This is an empty constructor everything parameter is set as null
     */
    public SubProgramNode() {
        this.name = null;
        this.parameters = new ParametersNode();
        this.variables = new DeclarationsNode();
        this.functions = new SubProgramDeclarationsNode();
        this.main = new CompoundStatementNode();
    }

    public String getName() {
        return this.name;
    }

    public void setName(String aName) {
        this.name = aName;
    }

    public ParametersNode getParameters() {
        return parameters;
    }

    public void setParameters(ParametersNode parameters) {
        this.parameters = parameters;
    }

    public DeclarationsNode getVariables() {
        return variables;
    }

    public void setVariables(DeclarationsNode variables) {
        this.variables = variables;
    }

    public SubProgramDeclarationsNode getFunctions() {
        return functions;
    }

    public void setFunctions(SubProgramDeclarationsNode functions) {
        this.functions = functions;
    }

    public CompoundStatementNode getMain() {
        return main;
    }

    public void setMain(CompoundStatementNode main) {
        this.main = main;
    }

    /**
     * Creates a String representation of this program node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "Sub Program: " + name + "\n";
        answer += parameters.indentedToString( level + 1);
        answer += variables.indentedToString( level + 1);
        answer += functions.indentedToString( level + 1);
        answer += main.indentedToString( level + 1);
        return answer;
    }
}
