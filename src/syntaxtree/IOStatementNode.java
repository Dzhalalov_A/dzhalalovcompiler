package syntaxtree;

public class IOStatementNode extends StatementNode{
    private String name;
    private String operation;
    private ExpressionNode attribute;

    public ExpressionNode getAttribute() {
        return attribute;
    }

    public void setAttribute(ExpressionNode attribute) {
        this.attribute = attribute;
    }

    public String getVarName() {
        return name;
    }

    public void setVarName(String name) {
        this.name = name;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }
    /**
     * Creates a String representation of this node and its children.
     * @param level The tree level at which this node resides.
     * @return A String representing this node.
     */
    @Override
    public String indentedToString( int level) {
        String answer = this.indentation( level);
        answer += "I/O " + this.operation + "\n";
        answer += this.attribute.indentedToString( level + 1);
        return answer;
    }
}
