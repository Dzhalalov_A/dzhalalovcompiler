package symboltable;

/** Associates IdKind with an integer value
    SymbolTable uses this enumerated values to assign different IDs their types.
 */
public enum IdType {
        PROGRAM, VARIABLE, ARRAY, FUNCTION,PROCEDURE;
}

