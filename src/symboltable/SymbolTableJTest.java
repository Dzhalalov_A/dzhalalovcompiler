package symboltable;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;

/**
 Unit tests of the SymbolTable class, to be run with JUnit.
*/
public class SymbolTableJTest {

    /**
     * addProgramName test
     */
    @Test
    public void testAddProgramName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addProgramName");
        st.addProgramName(testName);                                //adding lexeme to SymbolTable
        assertEquals(IdType.PROGRAM, st.idTable.get(testName));     //testing if added lexeme has correct IdType
        System.out.println("addProgram test passed!");
    }

    /**
     * addProgramName negative test
     */
    @Test
    public void negativeTestAddProgramName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addProgramName");
        st.addVariableName(testName);                               //adding lexeme of incorrect type to SymbolTable
        assertNotEquals(IdType.PROGRAM, st.idTable.get(testName));  //testing if added lexeme has incorrect IdType
        System.out.println("negative addProgram test passed!");
    }

    /**
     * addVariableName test
     */
    @Test
    public void testAddVariableName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addVariableName");
        st.addVariableName(testName);                               //adding lexeme to SymbolTable
        assertEquals(IdType.VARIABLE, st.idTable.get(testName));    //testing if added lexeme has correct IdType
        System.out.println("addVariableName test passed!");
    }

    /**
     * addVariableName negative test
     */
    @Test
    public void negativeTestAddVariableName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addVariableName");
        st.addArrayName(testName);                                  //adding lexeme of incorrect type to SymbolTable
        assertNotEquals(IdType.VARIABLE, st.idTable.get(testName)); //testing if added lexeme has incorrect IdType
        System.out.println("negative addVariableName test passed!");
    }


    /**
     * addArrayName test
     */
    @Test
    public void testAddArrayName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addArrayName");
        st.addArrayName(testName);                                  //adding lexeme to SymbolTable
        assertEquals(IdType.ARRAY, st.idTable.get(testName));       //testing if added lexeme has correct IdType
        System.out.println("addArrrayName test passed!");
    }

    /**
     * addArrayName negative test
     */
    @Test
    public void negativeTestAddArrayName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addArrayName");
        st.addProgramName(testName);                                //adding lexeme of incorrect type to SymbolTable
        assertNotEquals(IdType.ARRAY, st.idTable.get(testName));    //testing if added lexeme has incorrect IdType
        System.out.println("negative addArrrayName test passed!");
    }

    /**
     * addFunctionName test
     */
    @Test
    public void testAddFunctionName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addFunctionName");
        st.addFunctionName(testName);                               //adding lexeme to SymbolTable
        assertEquals(IdType.FUNCTION, st.idTable.get(testName));    //testing if added lexeme has correct IdType
        System.out.println("addFunctionName test passed!");
    }

    /**
     * addFunctionName negative test
     */
    @Test
    public void negativeTestAddFunctionName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addFunctionName");
        st.addProcedureName(testName);                              //adding lexeme of incorrect type to SymbolTable
        assertNotEquals(IdType.FUNCTION, st.idTable.get(testName)); //testing if added lexeme has incorrect IdType
        System.out.println("negative addFunctionName test passed!");
    }

    /**
     * addProcedureName test
     */
    @Test
    public void testAddProcedureName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addProcedureName");
        st.addProcedureName(testName);                              //adding lexeme to SymbolTable
        assertEquals(IdType.PROCEDURE, st.idTable.get(testName));   //testing if added lexeme has correct IdType
        System.out.println("addProcedureName test passed!");
    }

    /**
     * addProcedureName negative test
     */
    @Test
    public void negativeTestAddProcedureName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing addProcedureName");
        st.addFunctionName(testName);                               //adding lexeme of incorrect type to SymbolTable
        assertNotEquals(IdType.PROCEDURE, st.idTable.get(testName));//testing if added lexeme has incorrect IdType
        System.out.println("negative addProcedureName test passed!");
    }
    /**
     * isProgramName test
     */
    @Test
    public void testIsProgramName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isProgramName");
        st.addProgramName(testName);                                //adding lexeme to SymbolTable
        assertTrue(st.isProgramName(testName));                     //testing if a lexeme is of type PROGRAM
        System.out.println("isProgramName test passed!");
    }

    /**
     * isProgramName negative test
     */
    @Test
    public void negativeTestIsProgramName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isProgramName");
        st.addArrayName(testName);                                  //adding lexeme of incorrect type to SymbolTable
        assertFalse(st.isProgramName(testName));                    //testing if a lexeme is NOT of type PROGRAM
        System.out.println("negative isProgramName test passed!");
    }

    /**
     * isVariableName test
     */
    @Test
    public void testIsVariableName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isVariableName");
        st.addVariableName(testName);                               //adding lexeme to SymbolTable
        assertTrue(st.isVariableName(testName));                    //testing if a lexeme is of type VARIABLE
        System.out.println("isVariableName test passed!");
    }

    /**
     * isVariableName negative test
     */
    @Test
    public void negativeTestIsVariableName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isVariableName");
        st.addFunctionName(testName);                               //adding lexeme of incorrect type to SymbolTable
        assertFalse(st.isVariableName(testName));                   //testing if a lexeme is NOT of type VARIABLE
        System.out.println("negative isVariableName test passed!");
    }

    /**
     * isArrayName test
     */
    @Test
    public void testIsArrayName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isArrayName");
        st.addArrayName(testName);                                  //adding lexeme to SymbolTable
        assertTrue(st.isArrayName(testName));       //testing if a lexeme is of type ARRAY
        System.out.println("isArrayName test passed!");
    }

    /**
     * isArrayName negative test
     */
    @Test
    public void negativeTestIsArrayName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isArrayName");
        st.addVariableName(testName);                               //adding lexeme of incorrect type to SymbolTable
        assertFalse(st.isArrayName(testName));                      //testing if a lexeme is NOT of type ARRAY
        System.out.println("negative isArrayName test passed!");
    }

    /**
     * isFunctionName test
     */
    @Test
    public void testIsFunctionName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isFunctionName");
        st.addFunctionName(testName);                               //adding lexeme to SymbolTable
        assertTrue(st.isFunctionName(testName));    //testing if a lexeme is of type FUNCTION
        System.out.println("isFunctionName test passed!");
    }

    /**
     * isFunctionName negative test
     */
    @Test
    public void negativeTestIsFunctionName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isFunctionName");
        st.addProcedureName(testName);                              //adding lexeme of incorrect type to SymbolTable
        assertFalse(st.isFunctionName(testName));                   //testing if a lexeme is of type FUNCTION
        System.out.println("negative isFunctionName test passed!");
    }

    /**
     * isProcedureName test
     */
    @Test
    public void testIsProcedureName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isProcedureName");
        st.addProcedureName(testName);                              //adding lexeme to SymbolTable
        assertTrue(st.isProcedureName(testName));                   //testing if a lexeme is of type Procedure
        System.out.println("isProcedureName test passed!");
    }

    /**
     * isProcedureName negative test
     */
    @Test
    public void negativeTestIsProcedureName() {
        SymbolTable st = new SymbolTable();                         //An instance of SymbolTable
        String testName = new String("foo");                 //Identifier lexeme
        System.out.println("Testing isProcedureName");
        st.addFunctionName(testName);                               //adding lexeme of incorrect type to SymbolTable
        assertFalse(st.isProcedureName(testName));                  //testing if a lexeme is NOT of type Procedure
        System.out.println("negative isProcedureName test passed!");
    }
}
