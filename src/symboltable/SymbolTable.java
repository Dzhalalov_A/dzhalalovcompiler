package symboltable;
import java.util.HashMap;
import java.util.Map;

/**
 * Symbol Table is designed to keep track of different types of identifiers
 * The ability to distinguish between different types of identifiers wil give parser module efficient tool
 * to decipher among production rules of pascal grammar starting with same terminal symbol.
 * SymbolTable utilizes a HashMap idTable to store IdTypes of different identifiers
 * @author Artur Dzhalalov
 */

public class SymbolTable {

    HashMap<String, IdType> idTable;
    /**
     * Constructor creates a HashMap referred to as idTable
     */
    public SymbolTable() {
        idTable = new HashMap<String, IdType>(); //Creating an instance of HashMap to hold IdTypes
    }
    /**
     * addProgramName method adds an entry to HashMap idTable as follows: key - name, value - PROGRAM
     * @param name  lexeme part of ID token
     */
    public void addProgramName(String name){
        this.idTable.put(name, IdType.PROGRAM);
    }

    /**
     * addVariableName method adds an entry to HashMap idTable as follows: key - name, value - VARIABLE
     * @param name  lexeme part of ID token
     */
    public void addVariableName(String name){
        this.idTable.put(name, IdType.VARIABLE);
    }

    /**
     * addArrayName method adds an entry to HashMap idTable as follows: key - name, value - ARRAY
     * @param name  lexeme part of ID token
     */
    public void addArrayName(String name){
        this.idTable.put(name, IdType.ARRAY);
    }

    /**
     * addFunctionName method adds an entry to HashMap idTable as follows: key - name, value - FUNCTION
     * @param name  lexeme part of ID token
     */
    public void addFunctionName(String name){
        this.idTable.put(name, IdType.FUNCTION);
    }

    /**
     * addProcedureName method adds an entry to HashMap idTable as follows: key - name, value - PROCEDURE
     * @param name  lexeme part of ID token
     */
    public void addProcedureName(String name){
        this.idTable.put(name, IdType.PROCEDURE);
    }

    /**
     * Checks if identifier is of PROGRAM type
     *
     * @param name string part of current identifier
     * @return true if identifier is of PROGRAM type, false otherwise
     */
    public boolean isProgramName(String name){
        return this.idTable.get(name) == IdType.PROGRAM;
    }

    /**
     * Checks if identifier is of VARIABLE type
     *
     * @param name string part of current identifier
     * @return true if identifier is of VARIABLE type, false otherwise
     */
    public boolean isVariableName(String name){
        return this.idTable.get(name) == IdType.VARIABLE;
    }

    /**
     * Checks if identifier is of ARRAY type
     *
     * @param name string part of current identifier
     * @return true if identifier is of ARRAY type, false otherwise
     */
    public boolean isArrayName(String name){
        return this.idTable.get(name) == IdType.ARRAY;
    }

    /**
     * Checks if identifier is of FUNCTION type
     *
     * @param name string part of current identifier
     * @return true if identifier is of FUNCTION type, false otherwise
     */
    public boolean isFunctionName(String name){
        return this.idTable.get(name) == IdType.FUNCTION;
    }

    /**
     * Checks if identifier is of PROCEDURE type
     *
     * @param name string part of current identifier
     * @return true if identifier is of PROCEDURE type, false otherwise
     */
    public boolean isProcedureName(String name){
        return this.idTable.get(name) == IdType.PROCEDURE;
    }
    /**
     * ToString method for SymbolTable object.
     *
     * @return String String representation of a key value pairs in table form.
     */
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        //Header of the table
        for(int i = 0; i < 45; i++){
            sb.append("#");
        }
        sb.append("\n");
        sb.append(String.format("#%-20s # %-20s#\n", "Identifier", "Type"));
        for(int i = 0; i < 45; i++){
            sb.append("#");
        }
        sb.append("\n");
        //Body of the table
        for(Map.Entry<String, IdType> entry : idTable.entrySet()){
            sb.append(String.format("#%-20s # %-20s#\n", entry.getKey(),idTypeToString(entry.getValue())));
        }
        for(int i = 0; i < 45; i++){
            sb.append("#");
        }
        sb.append("\n");

        String outputTable = new String(sb);
        return outputTable;
    }
    /**
     * Transforms IdType into string.
     *
     * @param type IdType
     * @return String string representation of IdType
     */
    public String idTypeToString(IdType type){
        String output = new String();
        switch (type){
            case PROGRAM:
                return "Program";
            case VARIABLE:
                return "Variable";
            case ARRAY:
                return "Array";
            case FUNCTION:
                return "Function";
            case PROCEDURE:
                return "Procedure";
            default:
                return "Invalid ID Type";
        }
    }
}
