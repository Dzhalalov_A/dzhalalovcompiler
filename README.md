Java based Pascal compiler (JBPC)
This java project is a fully functional compiler which produces MIPS code for Pascal programming language.
List of folders with short description:
Documentation:
    SDD.doc, SDD.pdf - software design document of the compiler.
out: contains machine code of source files from src folder.
src:
    compiler - contains CompilerMain class that runs the compiler.
    parser - contains Parser module of the compiler.
    scanner - contains classes needed to read in a program.
    symboltable - contains classes which help to create and maintain a Symbol Table.
    syntaxtree - contains set of classes which represent different types of nodes needed to create a syntax tree of a program.
    *.jar - set of third party utilities used to create and test compilers source code.
    *.pas - sample input files.
    *.txt - sample output files.
    README.md - a file containing short description of the compiler project.